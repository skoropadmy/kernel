# Harno-kernel
Npm module that helps you to build your projects

## Install
```console
npm install harno-kernel
```

## Brief overview
Harno-kernel contains three main components:
- `the kernel` - the main script that can register and manage services
- `services` - a bunch of available service, that contains different logics, methods and etc. that can be run in browser, node, and other enviroments
- `wrappers` - a bunch of available wrapper. Wrapper is a js script that registers a bunch of services for specific needs. Like: node server, web canvas game, electron app and etc

You can require any of that component independently.
- `the kernel`:
```js
import Kernel from 'harno-kernel/dist/kernel' // or just 'harno-kernel'
```
- `services`:
```js
import ExpressService from 'harno-kernel/dist/services/node/express/express.service'
import ConfigService from 'harno-kernel/dist/services/shared/config/config.service'
```
- `wrappers`:
```js
import ServerKernel from 'harno-kernel/dist/wrappers/server/server.kernel'
```

## Docs
- the kernel: [docs/KERNEL.md](docs/KERNEL.md)
- available services: [docs/SERVICES.md](docs/SERVICES.md)
- available kernel wrappers: [docs/WRAPPERS.md](docs/WRAPPERS.md)

## File naming conventions
- `.service.` (`game.service.js`) - files that contain services (classes that would become kernel services)
- `.class.` (`block.class.js`) -  files that contain classes that are used in other classes
- `.management.` (`files.management.js`) - files that contain services that manage some internal data
- `.model.` (`player.model.js`) - files that contain services that manage data in databases
- `.test.` (`game.service.test.js`) - files that contain tests
- `.router.` (`game.router.js`) - files that define routes
- `.any.` (`game.params.js`) - files that do something specific


## License
[License](LICENSE)
