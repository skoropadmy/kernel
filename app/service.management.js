/**
 * Class that manages services
 */
export default class ServiceManagement {
  /**
   * Inits services object
   */
  constructor () {
    this._services = {}
  }

  /**
   * Registers service
   * @param {string} name service name
   * @param {Object} service service
   * @throws {Error} if service is alredy defined
   */
  register (name, service) {
    if (this._services[name]) {
      throw new Error(`Service "${name}" is already defined`)
    }
    this._services[name] = service
  }

  /**
   * Gets service
   * @param {string} name service name
   * @return {Object} service
   * @throws {Error} if service is not defined
   */
  get (name) {
    if (this._services[name]) {
      return this._services[name]
    } else {
      throw new Error(`Service "${name}" is not defined`)
    }
  }

  /**
   * Gets all services
   * @return {Object} all services
   */
  getAll () {
    return this._services
  }

  /**
   * Removes all services
   */
  removeAll () {
    this._services = {}
  }

  /**
   * Inits services dependencies
   * @param {Object<string, Array<string>>} dependenciesConfig config that contains all services dependencies
   * @throws {Error} if dependenciesConfig is not an object
   */
  initDependencies (dependenciesConfig) {
    if (typeof dependenciesConfig !== 'object') {
      throw new Error('Dependencies config must be an object')
    }

    for (const serviceName of Object.keys(dependenciesConfig)) {
      const serviceDependencies = {}

      for (const d of dependenciesConfig[serviceName]) {
        serviceDependencies[d] = this.get(d)
      }

      const service = this.get(serviceName)
      service._dependencies = serviceDependencies
      service._d = service._dependencies
    }
  }
}
