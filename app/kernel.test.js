/* global describe, it, afterEach */
import Kernel from './kernel'
import sinon from 'sinon'

const kernel = new Kernel()
const sandbox = sinon.createSandbox()

const Service = class {}

/**
 * @test {Kernel}
 */
describe('Kernel', () => {
  afterEach(() => {
    kernel.serviceManagement.removeAll()
    sandbox.restore()
  })

  /**
   * @test {Kernel#registerService}
   */
  it('should register service', async () => {
    sandbox.stub(kernel.serviceManagement, 'register')

    const serviceName = 'service'
    kernel.registerService(serviceName, Service)

    sinon.assert.calledOnce(kernel.serviceManagement.register)
    sinon.assert.calledWithExactly(kernel.serviceManagement.register, serviceName, new Service())
  })

  /**
   * @test {Kernel#getService}
   */
  it('should get service', async () => {
    sandbox.stub(kernel.serviceManagement, 'get')

    const serviceName = 'service'
    kernel.getService(serviceName)

    sinon.assert.calledOnce(kernel.serviceManagement.get)
    sinon.assert.calledWithExactly(kernel.serviceManagement.get, serviceName)
  })

  /**
   * @test {Kernel#initDependencies}
   */
  it('should init services dependencies', async () => {
    sandbox.stub(kernel.serviceManagement, 'initDependencies')

    const dependenciesConfig = { service: [ 'd1', 'd2' ] }
    kernel.initDependencies(dependenciesConfig)

    sinon.assert.calledOnce(kernel.serviceManagement.initDependencies)
    sinon.assert.calledWithExactly(kernel.serviceManagement.initDependencies, dependenciesConfig)
  })
})
