import ServiceManagement from './service.management'

/**
 * Class that manages services(classes) for js
 */
export default class Kernel {
  /**
   * Inits service management
   */
  constructor () {
    this._serviceManagement = new ServiceManagement()
  }

  /**
   * Inits services dependencies
   * @param {Object<string, Array<string>>} dependenciesConfig config that contains all services dependencies
   * @throws {Error} if dependenciesConfig is not an object
   */
  initDependencies (dependenciesConfig) {
    this.serviceManagement.initDependencies(dependenciesConfig)
  }

  /**
   * Registers service
   * @param {string} name service name
   * @param {class} Service service class
   * @throws {Error} if service is alredy defined
   */
  registerService (name, Service) {
    this.serviceManagement.register(name, new Service(this.serviceManagement))
  }

  /**
   * Gets service
   * @param {string} name service name
   * @return {Object} service
   * @throws {Error} if service is not defined
   */
  getService (name) {
    return this.serviceManagement.get(name)
  }

  /**
   * Returns services management service
   * @return {Object} services management service
   */
  get serviceManagement () {
    return this._serviceManagement
  }

  /**
   * Returns all registered services
   * @return {Object} all registered services
   */
  get s () {
    return this.serviceManagement.getAll()
  }
}
