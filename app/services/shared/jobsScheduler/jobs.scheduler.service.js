import schedule from 'node-schedule'

/**
 * Class that manages jobs (jobs are functions that behave like cron jobs)
 */
export default class JobsSchedulerService {
  /**
   * Inits service
   */
  init () {
    this._jobs = {}
    this._logger = this._d.logger.createLogger('JobsSchedulerService')
  }

  /**
   * Schedules a job
   * @param {string} name job name
   * @param {string} cron cron like string
   * @param {Function} job function that would be executed
   */
  scheduleJob (name, cron, job) {
    if (this._jobs[name]) {
      throw new Error(`Job name '${name}' is already taken`)
    }
    this._jobs[name] = schedule.scheduleJob(cron, job)
  }

  /**
   * Stops all jobs
   */
  stopAllJobs () {
    const keys = Object.keys(this._jobs)
    for (const name of keys) {
      this._jobs[name].cancel()
      delete this._jobs[name]
    }
  }

  /**
   * Stops one job
   * @param {string} name job name
   */
  stopJob (name) {
    if (!this._jobs[name]) {
      this._logger.warn(`Sorry , there is no jobs with name "${name}"`)
      return
    }
    this._jobs[name].cancel()
    delete this._jobs[name]
  }

  /**
   * Gets all jobs
   * @return {Object} all jobs
   */
  get jobs () {
    return this._jobs
  }
}
