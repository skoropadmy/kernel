/* global describe, it, beforeEach, afterEach */
import 'should'
import sinon from 'sinon'
import JobsSchedulerService from './jobs.scheduler.service'

const jobsScheduler = new JobsSchedulerService()
const logger = { createLogger () { return { warn () {} } } }
jobsScheduler._depencencies = jobsScheduler._d = { logger }
jobsScheduler.init()

const sandbox = sinon.createSandbox()

/**
 * @test {JobsSchedulerService}
 */
describe('JobsSchedulerService', () => {
  beforeEach(() => {
    sandbox.useFakeTimers(Date.now())
  })

  afterEach(() => {
    jobsScheduler.stopAllJobs()
    sandbox.restore()
  })

  /**
   * @test {JobsSchedulerService#scheduleJob}
   */
  it('should schedule jobs', () => {
    const set = new Set()

    jobsScheduler.scheduleJob('1', '* * * * * *', () => set.add(1))
    jobsScheduler.scheduleJob('2', '* * * * * *', () => set.add(2))
    jobsScheduler.scheduleJob('3', '* * * * * *', () => set.add(3))

    jobsScheduler.jobs.should.have.only.keys('1', '2', '3')

    sandbox.clock.tick(1500)

    set.size.should.be.exactly(3)
  })

  /**
   * @test {JobsSchedulerService#stopJob}
   */
  it('should stop one job at a time', () => {
    const set = new Set()

    jobsScheduler.scheduleJob('1', '* * * * * *', () => set.add(1))
    jobsScheduler.scheduleJob('2', '* * * * * *', () => set.add(2))
    jobsScheduler.scheduleJob('3', '* * * * * *', () => set.add(3))

    jobsScheduler.stopJob('1')
    jobsScheduler.jobs.should.have.only.keys('2', '3')

    jobsScheduler.stopJob('2')
    jobsScheduler.jobs.should.have.only.keys('3')

    jobsScheduler.stopJob('3')
    Object.keys(jobsScheduler.jobs).should.have.lengthOf(0)

    sandbox.clock.tick(1500)
    set.size.should.be.exactly(0)
  })

  /**
   * @test {JobsSchedulerService#stopJob}
   */
  it('should not throw an error when stoping unexisting a job', () => {
    jobsScheduler.stopJob('whatever')
  })

  /**
   * @test {JobsSchedulerService#stopAllJobs}
   */
  it('should stop all jobs at once', () => {
    const set = new Set()

    jobsScheduler.scheduleJob('1', '* * * * * *', () => set.add(1))
    jobsScheduler.scheduleJob('2', '* * * * * *', () => set.add(2))
    jobsScheduler.scheduleJob('3', '* * * * * *', () => set.add(3))

    jobsScheduler.stopAllJobs()
    Object.keys(jobsScheduler.jobs).should.have.lengthOf(0)

    sandbox.clock.tick(1500)
    set.size.should.be.exactly(0)
  })

  /**
   * @test {JobsSchedulerService#scheduleJob}
   */
  it('should throw an error if job is already created', () => {
    jobsScheduler.scheduleJob('1', '* * * * * *', () => {})

    ;(() => {
      jobsScheduler.scheduleJob('1', '* * * * * *', () => {})
    }).should.throw(new Error(`Job name '1' is already taken`))

    jobsScheduler.jobs.should.have.only.keys('1')
  })
})
