/**
 * Class that manages tick functions (functions that is executed per interval)
 */
export default class TickService {
  /**
   * Inits service
   */
  init () {
    this._ticks = {}
    this._logger = this._d.logger.createLogger('TickService')
  }

  /**
   * Creates tick
   * @param {string} name tick name
   * @param {number} interval tick interval
   * @param {Function} tick function that is executed per interval
   */
  createTick (name, interval, tick) {
    if (this._ticks[name]) {
      throw new Error(`Tick name '${name}' is already taken`)
    }
    this._ticks[name] = setInterval(tick, interval)
  }

  /**
   * Stops all ticks
   */
  stopAllTicks () {
    for (const name of Object.keys(this._ticks)) {
      clearInterval(this._ticks[name])
      delete this._ticks[name]
    }
  }

  /**
   * Stops one tick
   * @param {string} name tick name
   */
  stopTick (name) {
    if (!this._ticks[name]) {
      this._logger.warn(`Sorry, there is no tick with name "${name}"`)
      return
    }
    clearInterval(this._ticks[name])
    delete this._ticks[name]
  }

  /**
   * Gets all ticks
   * @return {Object} all ticks
   */
  get ticks () {
    return this._ticks
  }
}
