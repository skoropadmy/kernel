/* global describe, it, beforeEach, afterEach */
import 'should'
import sinon from 'sinon'
import TickService from './tick.service'

const tickService = new TickService()
const logger = { createLogger () { return { warn () {} } } }
tickService._depencencies = tickService._d = { logger }
tickService.init()

const sandbox = sinon.createSandbox()

/**
 * @test {TickService}
 */
describe('TickService', () => {
  beforeEach(() => {
    sandbox.useFakeTimers(Date.now())
  })

  afterEach(() => {
    tickService.stopAllTicks()
    sandbox.restore()
  })

  /**
   * @test {TickService#createTick}
   */
  it('should create ticks', () => {
    const counters = [0, 0, 0]

    tickService.createTick('tick1', 100, () => counters[0]++)
    tickService.createTick('tick2', 200, () => counters[1]++)
    tickService.createTick('tick3', 300, () => counters[2]++)

    tickService.ticks.should.have.only.keys('tick1', 'tick2', 'tick3')

    sandbox.clock.tick(1650)

    counters.should.be.eql([16, 8, 5])
  })

  /**
   * @test {TickService#stopTick}
   */
  it('should stop tick', () => {
    let counter = 0

    tickService.createTick('tick1', 100, () => counter++)
    sandbox.clock.tick(150)

    tickService.stopTick('tick1')
    sandbox.clock.tick(150)
    Object.keys(tickService.ticks).should.have.lengthOf(0)
    counter.should.be.eql(1)
  })

  /**
   * @test {TickService#stopTick}
   */
  it('should not throw an error when stoping unexisting a tick', () => {
    tickService.stopTick('whatever')
  })

  /**
   * @test {TickService#stopAllTicks}
   */
  it('should stop all ticks', () => {
    const counters = [0, 0, 0]

    tickService.createTick('tick1', 100, () => counters[0]++)
    tickService.createTick('tick2', 100, () => counters[1]++)
    tickService.createTick('tick3', 100, () => counters[2]++)
    sandbox.clock.tick(150)

    tickService.stopAllTicks()
    sandbox.clock.tick(150)

    Object.keys(tickService.ticks).should.have.lengthOf(0)
    counters.should.be.eql([1, 1, 1])
  })

  /**
   * @test {TickService#createTick}
   */
  it('should throw an error if tick is already created', () => {
    tickService.createTick('tick1', 100, () => {})

    ;(() => {
      tickService.createTick('tick1', 200, () => {})
    }).should.throw(new Error(`Tick name 'tick1' is already taken`))

    tickService.ticks.should.have.only.keys('tick1')
  })
})
