import yaml from 'js-yaml'
import _ from 'lodash'

/**
 * Class that manages configuration
 */
export default class ConfigService {
  /**
   * Sets the config value
   * @param {Object} config config value
   */
  setConfig (config) {
    this._config = config
  }

  /**
   * Sets yml to the config
   * @param {Object} yml yml string
   */
  setConfigYML (yml) {
    this.setConfig(this._convertYML(yml))
  }

  /**
   * Sets json to the config
   * @param {Object} json json string
   */
  setConfigJSON (json) {
    this.setConfig(this._convertJSON(json))
  }

  /**
   * Sets default values for config
   * @param {Object} defaults default values
   */
  setDefaults (defaults) {
    this._config = _.defaultsDeep(this._config, defaults)
  }

  /**
   * Sets default yml values for config
   * @param {Object} yml yml string
   */
  setDefaultsYML (yml) {
    this.setDefaults(this._convertYML(yml))
  }

  /**
   * Sets default json values for config
   * @param {Object} json json string
   */
  setDefaultsJSON (json) {
    this.setDefaults(this._convertJSON(json))
  }

  /**
   * Returns config
   * @return {Object} config
   */
  get config () {
    return this._config
  }

  /**
   * Returns config
   * @return {Object} config
   */
  get c () {
    return this._config
  }

  /**
   * Sets config
   */
  set config (config) {
    this._config = config
  }

  /**
   * Converts json to js object
   * @param {Object} json json string
   * @return {Object} converted json
   */
  _convertJSON (json) {
    return JSON.parse(json)
  }

  /**
   * Converts yml to js object
   * @param {Object} yml yml string
   * @return {Object} converted yml
   */
  _convertYML (yml) {
    return yaml.safeLoad(yml)
  }
}
