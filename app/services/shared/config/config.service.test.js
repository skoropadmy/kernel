/* global describe, it, beforeEach */
import should from 'should'

import ConfigService from './config.service'

const configService = new ConfigService()

/**
 * @test {ConfigService}
 */
describe('ConfigService', () => {
  beforeEach(() => {
    configService.config = undefined
  })

  /**
   * @test {ConfigService#setConfig}
   */
  it('should set config', async () => {
    configService.setConfig({ fruit0: ['cool'] })

    configService.c.should.deepEqual({ fruit0: ['cool'] })
  })

  /**
   * @test {ConfigService#setConfigJSON}
   */
  it('should set json config', async () => {
    configService.setConfigJSON('{"fruit3": ["apple","orange","banana"]}')

    configService.c.should.deepEqual({ fruit3: [ 'apple', 'orange', 'banana' ] })
  })

  /**
   * @test {ConfigService#setConfigYML}
   */
  it('should set yml config', async () => {
    configService.setConfigYML('fruit4:\n- apple\n- orange\n- banana')

    configService.c.should.deepEqual({ fruit4: [ 'apple', 'orange', 'banana' ] })
  })

  /**
   * @test {ConfigService#setDefaults}
   */
  it('should set default config values', async () => {
    configService.config = { fruit5: [ 'apple', 'orange', 'banana' ], anything: 78 }

    configService.setDefaults({ fruit5: [ 'apple', 'orange', 'banana', 'default1' ], default2: { default3: 45 } })

    configService.c.should.deepEqual({
      fruit5: [ 'apple', 'orange', 'banana', 'default1' ],
      default2: { default3: 45 },
      anything: 78
    })
  })

  /**
   * @test {ConfigService#setDefaultsJSON}
   */
  it('should set json defaults for config', async () => {
    configService.config = { fruit6: { prop2: 45 } }

    configService.setDefaultsJSON('{"fruit6": {"prop": 45}}')

    configService.c.should.deepEqual({ fruit6: { prop: 45, prop2: 45 } })
  })

  /**
   * @test {ConfigService#setDefaultsYML}
   */
  it('should set yml defaults for config', async () => {
    configService.config = { fruit7: { prop2: 45 } }

    configService.setDefaultsYML('fruit7:\n  prop: 45')

    configService.c.should.deepEqual({ fruit7: { prop: 45, prop2: 45 } })
  })

  /**
   * @test {ConfigService#config}
   * @test {ConfigService#c}
   */
  it('should set and get config', async () => {
    const config = { fruit6: 6 }

    configService.config = config

    should.deepEqual(configService.config, config)
    should.deepEqual(configService.c, configService.config)
  })
})
