import StringValidator from './validators/string.validator'
import NumberValidator from './validators/number.validator'
import ObjectValidator from './validators/object.validator'
import ArrayValidator from './validators/array.validator'
import ValueValidator from './validators/value.validator'
import LogicValidator from './validators/logic.validator'

class ValidationError extends Error {
  constructor (message, details) {
    super(message)
    this.name = 'ValidationError'
    this.details = details || []
  }
}

/**
 * Class that validates different values
 */
export default class ValidationService {
  constructor () {
    this._ValidationError = ValidationError
  }

  /**
   * Gets current ValidationError class
   * @return {Error} ValidationError class
   */
  get ValidationError () {
    return this._ValidationError
  }

  /**
   * Sets ValidationError class
   * @param {Error} SomeError error class to use instead of default error
   */
  set ValidationError (SomeError) {
    this._ValidationError = SomeError
  }

  // ########## STRING RELATED METHODS ##########

  /**
   * Validates string validator config and creates that validator
   * @param {Object} options string validator options
   * @return {Validator} string validator
   */
  isString (options) {
    this.isOptional(this.isObject({
      optional: {
        regexp: this.isRegexp(),
        url: this.isBoolean(),
        email: this.isBoolean(),
        strongPassword: this.isBoolean()
      }
    })).validate(options, 'validation.isStringOptions')

    return this._isString(options)
  }

  /**
   * Creates validator for url
   * @return {Validator} that validates url
   */
  isUrl () {
    return this.isString({ url: true })
  }

  /**
   * Creates validator for email
   * @return {Validator} that validates email
   */
  isEmail () {
    return this.isString({ email: true })
  }

  /**
   * Creates validator for strong password
   * @return {Validator} that validates strong password
   */
  isStrongPassword () {
    return this.isString({ strongPassword: true })
  }

  // ########## OBJECT RELATED METHODS ##########

  /**
   * Validates object validator config and creates that validator
   * @param {Object} options object validator options
   * @return {Validator} object validator
   */
  isObject (options) {
    this.isOptional(this._isObject({
      optional: {
        required: this._isObject({
          rest: {
            key: this._isString(),
            value: this._isValidator()
          }
        }),
        optional: this._isObject({
          rest: {
            key: this._isString(),
            value: this._isValidator()
          }
        }),
        rest: this._isObject({
          optional: {
            key: this._isValidator(),
            value: this._isValidator()
          }
        })
      }
    })).validate(options, 'validation.isObjectOptions')

    return this._isObject(options)
  }

  // ########## ARRAY RELATED METHODS ##########

  /**
   * Validates array validator config and creates that validator
   * @param {Object} options array validator options
   * @return {Validator} array validator
   */
  isArray (options) {
    this.isOptional(this._isObject({
      optional: {
        length: this._isValidator(),
        element: this._isValidator()
      }
    })).validate(options, 'validation.isArrayOptions')

    return new ArrayValidator(this, options)
  }

  // ########## NUMBER RELATED METHODS ##########

  /**
   * Validates number validator config and creates that validator
   * @param {Object} options number validator options
   * @return {Validator} number validator
   */
  isNumber (options) {
    this.isOptional(this.isObject({
      optional: {
        range: this.isArray({ element: this._isNumber(), length: this.isValue(2) }),
        integer: this.isBoolean(),
        min: this._isNumber(),
        max: this._isNumber()
      }
    })).validate(options, 'validation.isNumberOptions')

    return this._isNumber(options)
  }

  /**
   * Creates validator to check if number is in range
   * @param {number} start start of the range
   * @param {number} end end of the range
   * @return {Validator} that validates if number is in range
   */
  isInRange (start, end) {
    return this.isNumber({ range: [start, end] })
  }

  /**
   * Creates validator to check if number is an integer
   * @return {Validator} that validates if number is an integer
   */
  isInteger () {
    return this.isNumber({ integer: true })
  }

  // ########## VALUE RELATED METHODS ##########

  /**
   * Creates validator to check if value is not defined type
   * @param {string} notType js variable type
   * @return {Validator} that validates if value is not defined type
   */
  isNotType (notType) {
    return this._isValue({ notType })
  }

  /**
   * Creates validator to check if value is defined type
   * @param {string} type js variable type
   * @return {Validator} that validates if value is defined type
   */
  isType (type) {
    return this._isValue({ type })
  }

  /**
   * Creates validator to check if value is equal to defined value with ===
   * @param {Any} value any js value
   * @return {Validator} that validates if value is equal to defined value with ===
   */
  isValue (value) {
    return this._isValue({ value })
  }

  /**
   * Creates validator to check if value is a boolean
   * @return {Validator} that validates if value is a boolean
   */
  isBoolean () {
    return this.AND(this.exist(), this.isType('boolean'))
  }

  /**
   * Creates validator to check if value is a regexp
   * @return {Validator} that validates if value is a regexp
   */
  isRegexp () {
    return this.AND(this.exist(), this.isType('regexp'))
  }

  /**
   * Creates validator to check if value exists
   * @return {Validator} that validates if value exists
   */
  exist () {
    return this._isValue({ exist: true })
  }

  /**
   * Creates validator to check if value is null
   * @return {Validator} that validates if value is null
   */
  isNull () {
    return this.isType('null')
  }

  /**
   * Creates validator to check if value is undefined
   * @return {Validator} that validates if value is undefined
   */
  isUndefined () {
    return this.isType('undefined')
  }

  // ########## LOGIC RELATED METHODS ##########

  /**
   * Creates validator that combines other validators into one with bitwise operator AND
   * @param {...Validator} validators to be combined together
   * @return {Validator} that validates conbined validators
   */
  AND (...validators) {
    return new LogicValidator(this, { operator: 'AND', validators })
  }

  /**
   * Creates validator that combines other validators into one with bitwise operator OR
   * @param {...Validator} validators to be combined together
   * @return {Validator} that validates conbined validators
   */
  OR (...validators) {
    return new LogicValidator(this, { operator: 'OR', validators })
  }

  /**
   * Creates validator that doesn't throw an error if value is undefiend
   * @param {...Validator} validators validators to be combined together
   * @return {Validator} that doesn't throw an error if value is undefined
   */
  isOptional (...validators) {
    return new LogicValidator(this, { optional: true, validators })
  }

  // ########## CUSTOM RELATED METHODS ##########

  /**
   * Creates validator with custom function to test value
   * @param {Function(value, path): boolean} callback test passed if returns true
   * @return {Validator} that validates value with custom function
   */
  customTest (callback) {
    return this._isValue({ customTest: callback })
  }

  /**
   * Creates validator with custom validate function
   * @param {Function(value, path)} validate should throw an error if validation fails
   * @return {Validator} with custom validate function
   */
  customValidator (validate) {
    return { validate }
  }

  // ########## OTHER METHODS ##########

  /**
   * Executes validators and throws combined error if any validator falils
   * @param {Array<Validator>} validators validators
   * @param {Array<Any>} values values to validate
   * @param {Array<string>} [paths=[]] paths to values
   * @throw {ValidationError} if any validator fails
   */
  executeValidatorsWithValues (validators, values, paths = []) {
    const errors = []

    for (const [i, validator] of validators.entries()) {
      try {
        validator.validate(values[i], paths[i])
      } catch (e) {
        this.handleValidationError(e)
        errors.push(e)
      }
    }

    if (errors.length > 0) {
      throw this.combinedValidationErrors(...errors)
    }
  }

  /**
   * Combines validation errors into one
   * @param {...ValidatorError} errors validation errors
   * @return {ValidationError} combined error
   */
  combinedValidationErrors (...errors) {
    const details = []
    let messages = ''

    for (const [i, e] of errors.entries()) {
      details.push(...e.details)
      messages += `${i + 1}: ${e.message} | `
    }

    messages = messages.slice(0, -3)

    return new this._ValidationError(messages, details)
  }

  /**
   * Handles the error. If error is not ValidationError throw it
   * @param {Error} error error to check
   * @throw {Error} if error is not ValidationError
   */
  handleValidationError (error) {
    if (!(error instanceof this._ValidationError)) {
      throw error
    }
  }

  /**
   * Create value validator
   * @param {Object} options value validator options
   * @return {Validator} value validator
   */
  _isValue (options) {
    return new ValueValidator(this, options)
  }

  /**
   * Create object validator
   * @param {Object} options object validator options
   * @return {Validator} object validator
   */
  _isObject (options) {
    return new ObjectValidator(this, options)
  }

  /**
   * Create string validator
   * @param {Object} options string validator options
   * @return {Validator} string validator
   */
  _isString (options) {
    return new StringValidator(this, options)
  }

  /**
   * Create string validator
   * @param {Object} options string validator options
   * @return {Validator} string validator
   */
  _isNumber (options) {
    return new NumberValidator(this, options)
  }

  /**
   * Create validator validator
   * @return {Validator} validator validator
   */
  _isValidator () {
    // needs to be improved
    return this.isType('object') && this.customTest((v) => { return typeof v.validate === 'function' })
  }
}
