/* global describe, it */
import 'should'
import LogicValidator from './logic.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {LogicValidator}
 */
describe('LogicValidator', () => {
  /**
   * @test {LogicValidator#validate}
   */
  it('should return if no options specified', async () => {
    const logicValidator = new LogicValidator(validation)

    logicValidator.validate('anything', '')
  })

  /**
   * @test {LogicValidator#validate}
   */
  it('should validate logic with path', async () => {
    const logicValidator = new LogicValidator(validation, { optional: true, validators: [validation.isString()] })

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { logicValidator.validate({}, path) },
      'Invalid value. Optional value must be valid or undefined',
      [{
        message: 'Incorrect type. Expected string',
        value: {},
        path
      }]
    )
  })

  /**
   * @test {LogicValidator#validate}
   */
  it('should validate with AND logic', async () => {
    const logicValidator = new LogicValidator(validation, {
      operator: 'AND',
      validators: [validation.isType('number'), validation.isValue(54)]
    })

    logicValidator.validate(54)

    validatorThrow(
      () => { logicValidator.validate(200, '') },
      'Invalid value. Logic operator AND failed',
      [{
        message: 'Value must strict equal: 54',
        value: 200,
        path: ''
      }]
    )
  })

  /**
   * @test {LogicValidator#validate}
   */
  it('should validate with OR logic', async () => {
    const logicValidator = new LogicValidator(validation, {
      operator: 'OR',
      validators: [validation.isType('string'), validation.isValue(54)]
    })

    logicValidator.validate(54)

    validatorThrow(
      () => { logicValidator.validate({}, '') },
      'Invalid value. Logic operator OR failed',
      [{
        message: 'Incorrect type. Expected string',
        value: {},
        path: ''
      }, {
        message: 'Value must strict equal: 54',
        value: {},
        path: ''
      }]
    )
  })

  /**
   * @test {LogicValidator#validate}
   */
  it('should validate value but don\'t throw if it is an optional', async () => {
    const logicValidator = new LogicValidator(validation, {
      optional: true,
      validators: [validation.isType('string')]
    })

    logicValidator.validate(undefined)

    validatorThrow(
      () => { logicValidator.validate(10, '') },
      'Invalid value. Optional value must be valid or undefined',
      [{
        message: 'Incorrect type. Expected string',
        value: 10,
        path: ''
      }]
    )
  })
})
