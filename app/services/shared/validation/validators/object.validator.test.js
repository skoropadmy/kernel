/* global describe, it */
import 'should'
import ObjectValidator from './object.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {ObjectValidator}
 */
describe('ObjectValidator', () => {
  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object with path', async () => {
    const objectValidator = new ObjectValidator(validation)

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { objectValidator.validate([], path) },
      'Incorrect type. Expected object',
      [{
        message: 'Incorrect type. Expected object',
        value: [],
        path
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate if object type is object', async () => {
    const objectValidator = new ObjectValidator(validation)

    objectValidator.validate({})

    validatorThrow(
      () => { objectValidator.validate([], '') },
      'Incorrect type. Expected object',
      [{
        message: 'Incorrect type. Expected object',
        value: [],
        path: ''
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate if object exists', async () => {
    const objectValidator = new ObjectValidator(validation)

    objectValidator.validate({})

    validatorThrow(
      () => { objectValidator.validate(undefined, '') },
      'Value must exist',
      [{
        message: 'Value must exist',
        value: undefined,
        path: ''
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object required parameters', async () => {
    const objectValidator = new ObjectValidator(validation, {
      required: {
        prop1: validation.isString(),
        prop2: validation.isNumber()
      }
    })

    objectValidator.validate({ prop1: 'string', prop2: 234 })

    validatorThrow(
      () => { objectValidator.validate({ prop1: 'string' }) },
      'Invalid object parameters',
      [{
        message: 'Value must exist',
        value: undefined,
        path: '.prop2'
      }]
    )

    validatorThrow(
      () => { objectValidator.validate({ prop1: 41, prop2: 65 }) },
      'Invalid object parameters',
      [{
        message: 'Incorrect type. Expected string',
        value: 41,
        path: '.prop1'
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object optional parameters', async () => {
    const objectValidator = new ObjectValidator(validation, {
      optional: {
        prop1: validation.isString(),
        prop2: validation.isNumber()
      }
    })

    objectValidator.validate({ prop1: 'string' })

    validatorThrow(
      () => { objectValidator.validate({ prop2: 'string' }) },
      'Invalid object parameters',
      [{
        message: 'Incorrect type. Expected number',
        value: 'string',
        path: '.prop2'
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object with unexpected parameters', async () => {
    const objectValidator = new ObjectValidator(validation, {
      optional: {
        prop1: validation.isString()
      }
    })

    objectValidator.validate({})

    validatorThrow(
      () => { objectValidator.validate({ unexpected: 'string' }) },
      'Invalid object parameters',
      [{
        message: 'Unexpected parameter',
        value: 'string',
        path: '.unexpected'
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object keys with rest key validator', async () => {
    const objectValidator = new ObjectValidator(validation, {
      rest: {
        key: validation.isString({ regexp: /https?/ })
      }
    })

    objectValidator.validate({ http: 'anything' })

    validatorThrow(
      () => { objectValidator.validate({ https: 'anything', property: 'anything' }) },
      'Invalid object parameters',
      [{
        message: 'Invalid value. Value must match required pattern: /https?/',
        value: 'property',
        path: '.property{key}'
      }]
    )
  })

  /**
   * @test {ObjectValidator#validate}
   */
  it('should validate object values with rest value validator', async () => {
    const objectValidator = new ObjectValidator(validation, {
      rest: {
        value: validation.isNumber()
      }
    })

    objectValidator.validate({ anything: 41 })

    validatorThrow(
      () => { objectValidator.validate({ anything: 'string', anything2: 45 }) },
      'Invalid object parameters',
      [{
        message: 'Incorrect type. Expected number',
        value: 'string',
        path: '.anything'
      }]
    )
  })
})
