/**
 * Class that create bitwise logic for combination of validators
 */
export default class LogicValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options bitwise validator options
   */
  constructor (validationService, options) {
    this._options = options
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    if (!this._options) return

    const details = []
    const result = []

    for (const validator of this._options.validators) {
      try {
        validator.validate(value, path)
        result.push(true)
      } catch (e) {
        this._validationService.handleValidationError(e)
        details.push(...e.details)
        result.push(false)
      }
    }

    if (this._options.operator && this._options.operator.toUpperCase() === 'AND') {
      if (!result.every((r) => r)) {
        const message = `Invalid value. Logic operator ${this._options.operator} failed`
        throw new this._ValidationError(message, details)
      }
    }

    if (this._options.operator && this._options.operator.toUpperCase() === 'OR') {
      if (!result.some((r) => r)) {
        const message = `Invalid value. Logic operator ${this._options.operator} failed`
        throw new this._ValidationError(message, details)
      }
    }

    if (this._options.optional && value !== undefined) {
      if (!result.some((r) => r)) {
        const message = `Invalid value. Optional value must be valid or undefined`
        throw new this._ValidationError(message, details)
      }
    }
  }
}
