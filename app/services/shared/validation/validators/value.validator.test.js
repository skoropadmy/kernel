/* global describe, it */
import 'should'
import ValueValidator from './value.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {ValueValidator}
 */
describe('ValueValidator', () => {
  /**
   * @test {ValueValidator#validate}
   */
  it('should return if no options specified', async () => {
    const valueValidator = new ValueValidator(validation)

    valueValidator.validate('anything', '')
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate value with path', async () => {
    const valueValidator = new ValueValidator(validation, { notType: 'undefined' })

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { valueValidator.validate(undefined, path) },
      'Incorrect type. Expected NOT undefined',
      [{
        message: 'Incorrect type. Expected NOT undefined',
        value: undefined,
        path
      }]
    )
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate if value type equals defined type', async () => {
    const valueValidator = new ValueValidator(validation, { type: 'regexp' })

    valueValidator.validate(/reg/g)

    validatorThrow(
      () => { valueValidator.validate({}, '') },
      'Incorrect type. Expected regexp',
      [{
        message: 'Incorrect type. Expected regexp',
        value: {},
        path: ''
      }]
    )
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate if value type not equals defined type', async () => {
    const valueValidator = new ValueValidator(validation, { notType: 'array' })

    valueValidator.validate('string')

    validatorThrow(
      () => { valueValidator.validate([], '') },
      'Incorrect type. Expected NOT array',
      [{
        message: 'Incorrect type. Expected NOT array',
        value: [],
        path: ''
      }]
    )
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate if value exists', async () => {
    const valueValidator = new ValueValidator(validation, { exist: true })

    valueValidator.validate('exist')

    validatorThrow(
      () => { valueValidator.validate(null, '') },
      'Value must exist',
      [{
        message: 'Value must exist',
        value: null,
        path: ''
      }]
    )
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate if value equals defined value', async () => {
    const valueValidator = new ValueValidator(validation, { value: 'string' })

    valueValidator.validate('string')

    validatorThrow(
      () => { valueValidator.validate('other_string', '') },
      'Value must strict equal: string',
      [{
        message: 'Value must strict equal: string',
        value: 'other_string',
        path: ''
      }]
    )
  })

  /**
   * @test {ValueValidator#validate}
   */
  it('should validate value with custom function', async () => {
    const customTest = (value) => {
      if (value === 2) return true
      else return false
    }
    const valueValidator = new ValueValidator(validation, { customTest })

    valueValidator.validate(2)

    validatorThrow(
      () => { valueValidator.validate(3, '') },
      'Invalid value. Custom test failed',
      [{
        message: 'Invalid value. Custom test failed',
        value: 3,
        path: ''
      }]
    )
  })
})
