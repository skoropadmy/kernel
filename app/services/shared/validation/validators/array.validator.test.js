/* global describe, it */
import 'should'
import ArrayValidator from './array.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {ArrayValidator}
 */
describe('ArrayValidator', () => {
  /**
   * @test {ArrayValidator#validate}
   */
  it('should validate array with path', async () => {
    const arrayValidator = new ArrayValidator(validation)

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { arrayValidator.validate({}, path) },
      'Incorrect type. Expected array',
      [{
        message: 'Incorrect type. Expected array',
        value: {},
        path
      }]
    )
  })

  /**
   * @test {ArrayValidator#validate}
   */
  it('should validate if array type is array', async () => {
    const arrayValidator = new ArrayValidator(validation)

    arrayValidator.validate([])

    validatorThrow(
      () => { arrayValidator.validate({}, '') },
      'Incorrect type. Expected array',
      [{
        message: 'Incorrect type. Expected array',
        value: {},
        path: ''
      }]
    )
  })

  /**
   * @test {ArrayValidator#validate}
   */
  it('should validate if array exists', async () => {
    const arrayValidator = new ArrayValidator(validation)

    arrayValidator.validate([])

    validatorThrow(
      () => { arrayValidator.validate(undefined, '') },
      'Value must exist',
      [{
        message: 'Value must exist',
        value: undefined,
        path: ''
      }]
    )
  })

  /**
   * @test {ArrayValidator#validate}
   */
  it('should validate array elements', async () => {
    const arrayValidator = new ArrayValidator(validation, { element: validation.isString() })

    arrayValidator.validate(['string', 'string'])

    validatorThrow(
      () => { arrayValidator.validate(['string', 154, true]) },
      'Invalid array elements',
      [{
        message: 'Incorrect type. Expected string',
        value: 154,
        path: '[1]'
      }, {
        message: 'Incorrect type. Expected string',
        value: true,
        path: '[2]'
      }]
    )
  })

  /**
   * @test {ArrayValidator#validate}
   */
  it('should validate array length', async () => {
    const arrayValidator = new ArrayValidator(validation, { length: validation.isInRange(4, 6) })

    arrayValidator.validate(['s', 's', 's', 's', 's'])

    validatorThrow(
      () => { arrayValidator.validate(['s', 's', 's']) },
      'Invalid array elements',
      [{
        message: 'Invalid value. Value must be in range: [4,6]',
        value: 3,
        path: '.length'
      }]
    )
  })
})
