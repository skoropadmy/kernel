import { isUri } from 'valid-url'

/**
 * Class that validates string values
 */
export default class StringValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options string validator options
   */
  constructor (validationService, options) {
    this._options = options
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    this._validationService.exist().validate(value, path)
    this._validationService.isType('string').validate(value, path)

    if (!this._options) return

    if (this._options.regexp) this._regexp(value, path)
    if (this._options.url) this._url(value, path)
    if (this._options.strongPassword) this._strongPassword(value, path)
    if (this._options.email) this._email(value, path)
  }

  /**
   * Validates if string is regexp
   * @param {Any} value any js value
   * @param {string} path path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _regexp (value, path) {
    if (!this._options.regexp.test(value)) {
      const message = `Invalid value. Value must match required pattern: ${this._options.regexp}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }

  /**
   * Validates if string is url
   * @param {Any} value any js value
   * @param {string} path path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _url (value, path) {
    if (!isUri(value)) {
      const message = `Invalid value. Value must be an url`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (value.endsWith('/')) {
      const message = `Invalid value. Url must NOT end with backslash '/'`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }

  /**
   * Validates if string is strong password
   * @param {Any} value any js value
   * @param {string} path path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _strongPassword (value, path) {
    if (value.length < 15) {
      const message = `Invalid value. Strong password must have at least 15 characters`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (!/(?=.*[!"?$%^&*()_\-+={[}\]:;@'~#|\\<,>.?/])/.test(value)) {
      const message = `Invalid value. Strong password must have symbos, such as ! " ? $ % ^ & * ( ) _ - + = { [ } ] : ; @ ' ~ # | < , > . ? /`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (!/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/.test(value)) {
      const message = `Invalid value. Strong password must have uppercase, lowercase letters and numbers`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }

  /**
   * Validates if string is email
   * @param {Any} value any js value
   * @param {string} path path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _email (value, path) {
    const emailRegexp = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i

    if (!emailRegexp.test(value)) {
      const message = `Invalid value. Wrong email syntax`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }
}
