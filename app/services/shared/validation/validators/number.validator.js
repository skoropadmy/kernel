/**
 * Class that validates number values
 */
export default class NumberValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options number validator options
   */
  constructor (validationService, options) {
    this._options = options
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    this._validationService.exist().validate(value, path)
    this._validationService.isType('number').validate(value, path)

    if (!this._options) return

    if (this._options.range && !(
      this._options.range[0] <= value && value <= this._options.range[1]
    )) {
      const message = `Invalid value. Value must be in range: [${this._options.range}]`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (this._options.integer && !/^-?\d+$/.test(value)) {
      const message = `Invalid value. Value must be an integer`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (typeof this._options.min === 'number' && this._options.min > value) {
      const message = `Invalid value. Value must be more then ${this._options.min}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (typeof this._options.max === 'number' && this._options.max < value) {
      const message = `Invalid value. Value must be less then ${this._options.max}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }
}
