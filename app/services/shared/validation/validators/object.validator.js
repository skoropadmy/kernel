/**
 * Class that validates objects
 */
export default class ObjectValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options object validator options
   */
  constructor (validationService, options) {
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
    this._options = options || {}

    if (!this._options.required) this._options.required = {}
    if (!this._options.optional) this._options.optional = {}
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    this._validationService.exist().validate(value, path)
    this._validationService.isType('object').validate(value, path)

    const parameters = new Set([
      ...Object.keys(value),
      ...Object.keys(this._options.required),
      ...Object.keys(this._options.optional)
    ])

    const details = []

    for (const parameter of parameters) {
      const parameterPath = `${path}.${parameter}`
      const parameterRequiredValidator = this._options.required[parameter]
      const parameterOptionalValidator = this._options.optional[parameter]
      const parameterValue = value[parameter]

      try {
        if (!parameterRequiredValidator && !parameterOptionalValidator) {
          if (this._options.rest) {
            const restKeyValidator = this._options.rest.key
            const restValueValidator = this._options.rest.value

            if (restKeyValidator) {
              restKeyValidator.validate(parameter, `${parameterPath}{key}`)
            }
            if (restValueValidator) {
              restValueValidator.validate(parameterValue, parameterPath)
            }
          } else {
            const message = 'Unexpected parameter'
            throw new this._ValidationError(message, [{
              message,
              value: parameterValue,
              path: parameterPath
            }])
          }
        }

        if (parameterRequiredValidator) {
          parameterRequiredValidator.validate(parameterValue, parameterPath)
        } else if (parameterOptionalValidator && parameterValue !== undefined) {
          parameterOptionalValidator.validate(parameterValue, parameterPath)
        }
      } catch (e) {
        this._validationService.handleValidationError(e)
        details.push(...e.details)
      }
    }

    if (details.length > 0) {
      const message = 'Invalid object parameters'
      throw new this._ValidationError(message, details)
    }
  }
}
