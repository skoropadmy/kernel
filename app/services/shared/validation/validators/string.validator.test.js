/* global describe, it */
import 'should'
import StringValidator from './string.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {StringValidator}
 */
describe('StringValidator', () => {
  /**
   * @test {StringValidator#validate}
   */
  it('should validate string with path', async () => {
    const stringValidator = new StringValidator(validation)

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { stringValidator.validate({}, path) },
      'Incorrect type. Expected string',
      [{
        message: 'Incorrect type. Expected string',
        value: {},
        path
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string type is string', async () => {
    const stringValidator = new StringValidator(validation)

    stringValidator.validate('string')

    validatorThrow(
      () => { stringValidator.validate({}, '') },
      'Incorrect type. Expected string',
      [{
        message: 'Incorrect type. Expected string',
        value: {},
        path: ''
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string exists', async () => {
    const stringValidator = new StringValidator(validation)

    stringValidator.validate('string')

    validatorThrow(
      () => { stringValidator.validate(undefined, '') },
      'Value must exist',
      [{
        message: 'Value must exist',
        value: undefined,
        path: ''
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string matches regexp', async () => {
    const stringValidator = new StringValidator(validation, { regexp: /string/ })

    stringValidator.validate('string')

    validatorThrow(
      () => { stringValidator.validate('', '') },
      'Invalid value. Value must match required pattern: /string/',
      [{
        message: 'Invalid value. Value must match required pattern: /string/',
        value: '',
        path: ''
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string is url and ends with \'/\'', async () => {
    const stringValidator = new StringValidator(validation, { url: true })

    stringValidator.validate('http://localhost:251')

    validatorThrow(
      () => { stringValidator.validate('not_url', '') },
      'Invalid value. Value must be an url',
      [{
        message: 'Invalid value. Value must be an url',
        value: 'not_url',
        path: ''
      }]
    )

    validatorThrow(
      () => { stringValidator.validate('http://localhost:251/', '') },
      'Invalid value. Url must NOT end with backslash \'/\'',
      [{
        message: 'Invalid value. Url must NOT end with backslash \'/\'',
        value: 'http://localhost:251/',
        path: ''
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string is strong password', async () => {
    const stringValidator = new StringValidator(validation, { strongPassword: true })

    stringValidator.validate('strongPASSWORD14745@##$@%')

    validatorThrow(
      () => { stringValidator.validate('small', '') },
      'Invalid value. Strong password must have at least 15 characters',
      [{
        message: 'Invalid value. Strong password must have at least 15 characters',
        value: 'small',
        path: ''
      }]
    )

    validatorThrow(
      () => { stringValidator.validate('withoutSYMBOLS234', '') },
      `Invalid value. Strong password must have symbos, such as ! " ? $ % ^ & * ( ) _ - + = { [ } ] : ; @ ' ~ # | < , > . ? /`,
      [{
        message: `Invalid value. Strong password must have symbos, such as ! " ? $ % ^ & * ( ) _ - + = { [ } ] : ; @ ' ~ # | < , > . ? /`,
        value: 'withoutSYMBOLS234',
        path: ''
      }]
    )

    validatorThrow(
      () => { stringValidator.validate('wrongpatternbutbigenough#$', '') },
      'Invalid value. Strong password must have uppercase, lowercase letters and numbers',
      [{
        message: 'Invalid value. Strong password must have uppercase, lowercase letters and numbers',
        value: 'wrongpatternbutbigenough#$',
        path: ''
      }]
    )
  })

  /**
   * @test {StringValidator#validate}
   */
  it('should validate if string is email', async () => {
    const stringValidator = new StringValidator(validation, { email: true })

    stringValidator.validate('some@valid.email')

    validatorThrow(
      () => { stringValidator.validate('not@valid#email', '') },
      'Invalid value. Wrong email syntax',
      [{
        message: 'Invalid value. Wrong email syntax',
        value: 'not@valid#email',
        path: ''
      }]
    )
  })
})
