/**
 * Class that validates array values
 */
export default class ArrayValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options array validator options
   */
  constructor (validationService, options) {
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
    this._options = options
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    this._validationService.exist().validate(value, path)
    this._validationService.isType('array').validate(value, path)

    if (!this._options) return

    const details = []

    if (this._options.length) {
      try {
        this._options.length.validate(value.length, `${path}.length`)
      } catch (e) {
        this._validationService.handleValidationError(e)
        details.push(...e.details)
      }
    }

    if (this._options.element) {
      for (const [i, element] of value.entries()) {
        try {
          this._options.element.validate(element, `${path}[${i}]`)
        } catch (e) {
          this._validationService.handleValidationError(e)
          details.push(...e.details)
        }
      }
    }

    if (details.length > 0) {
      const message = 'Invalid array elements'
      throw new this._ValidationError(message, details)
    }
  }
}
