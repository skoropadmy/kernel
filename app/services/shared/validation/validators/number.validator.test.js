/* global describe, it */
import 'should'
import NumberValidator from './number.validator'
import ValidationService from '../validation.service'

const validation = new ValidationService()
const ValidationError = validation._ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {NumberValidator}
 */
describe('NumberValidator', () => {
  /**
   * @test {NumberValidator#validate}
   */
  it('should validate number with path', async () => {
    const numberValidator = new NumberValidator(validation)

    const path = 'some.path[0].someWhere'
    validatorThrow(
      () => { numberValidator.validate({}, path) },
      'Incorrect type. Expected number',
      [{
        message: 'Incorrect type. Expected number',
        value: {},
        path
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number type is number', async () => {
    const numberValidator = new NumberValidator(validation)

    // should not throw
    numberValidator.validate(10)

    // should throw
    validatorThrow(
      () => { numberValidator.validate({}, '') },
      'Incorrect type. Expected number',
      [{
        message: 'Incorrect type. Expected number',
        value: {},
        path: ''
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number exists', async () => {
    const numberValidator = new NumberValidator(validation)

    // should not throw
    numberValidator.validate(30)

    // should throw
    validatorThrow(
      () => { numberValidator.validate(undefined, '') },
      'Value must exist',
      [{
        message: 'Value must exist',
        value: undefined,
        path: ''
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number is in range', async () => {
    const numberValidator = new NumberValidator(validation, { range: [10, 40] })

    // should not throw
    numberValidator.validate(30)

    // should throw
    validatorThrow(
      () => { numberValidator.validate(65, '') },
      'Invalid value. Value must be in range: [10,40]',
      [{
        message: 'Invalid value. Value must be in range: [10,40]',
        value: 65,
        path: ''
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number is an integer', async () => {
    const numberValidator = new NumberValidator(validation, { integer: true })

    // should not throw
    numberValidator.validate(30)

    // should throw
    validatorThrow(
      () => { numberValidator.validate(65.415, '') },
      'Invalid value. Value must be an integer',
      [{
        message: 'Invalid value. Value must be an integer',
        value: 65.415,
        path: ''
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number is more then minimum value', async () => {
    const numberValidator = new NumberValidator(validation, { min: 10 })

    // should not throw
    numberValidator.validate(30)

    // should throw
    validatorThrow(
      () => { numberValidator.validate(4, '') },
      'Invalid value. Value must be more then 10',
      [{
        message: 'Invalid value. Value must be more then 10',
        value: 4,
        path: ''
      }]
    )
  })

  /**
   * @test {NumberValidator#validate}
   */
  it('should validate if number is less then maximum value', async () => {
    const numberValidator = new NumberValidator(validation, { max: 10 })

    // should not throw
    numberValidator.validate(4)

    // should throw
    validatorThrow(
      () => { numberValidator.validate(30, '') },
      'Invalid value. Value must be less then 10',
      [{
        message: 'Invalid value. Value must be less then 10',
        value: 30,
        path: ''
      }]
    )
  })
})
