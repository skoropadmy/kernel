/**
 * Class that validates values in different ways
 */
export default class ValueValidator {
  /**
   * Assign options and validationService to the instance
   * @param {Object} validationService validationService
   * @param {Object} options value validator options
   */
  constructor (validationService, options) {
    this._validationService = validationService
    this._ValidationError = validationService._ValidationError
    this._options = options
  }

  /**
   * Returns function that validates the value
   * @return {Function(value, path)} function that validates the value
   */
  get validate () {
    return this._validate.bind(this)
  }

  /**
   * Validates the value
   * @param {Any} value any js value
   * @param {string} [path=''] path to that value
   * @throw {ValidationError} if validation doesn't pass
   */
  _validate (value, path = '') {
    if (!this._options) return

    if (this._options.type && !this._isType(value, this._options.type)) {
      const message = `Incorrect type. Expected ${this._options.type}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (this._options.notType && this._isType(value, this._options.notType)) {
      const message = `Incorrect type. Expected NOT ${this._options.notType}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (this._options.exist && (value === undefined || value === null)) {
      const message = 'Value must exist'
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (this._options.value && value !== this._options.value) {
      const message = `Value must strict equal: ${this._options.value}`
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }

    if (this._options.customTest && !this._options.customTest(value, path)) {
      const message = 'Invalid value. Custom test failed'
      const details = [{ message, value, path }]
      throw new this._ValidationError(message, details)
    }
  }

  /**
   * Validates type of the value
   * @param {Any} value any js value
   * @param {string} expectedType expected js variable type
   * @throw {ValidationError} if validation doesn't pass
   */
  _isType (value, expectedType) {
    expectedType = expectedType.toLowerCase()
    const valueType = Object.prototype.toString.call(value).slice(8, -1).toLowerCase()

    return valueType === expectedType
  }
}
