/* global describe, it */
import ValidationService from './validation.service'

const validation = new ValidationService()
const ValidationError = validation.ValidationError

const validatorThrow = (func, message, details) => {
  const error = new ValidationError(message, details)
  ;(() => { func() }).should.throw(error)
}

/**
 * @test {ValidationService}
 */
describe('ValidationService', () => {
  /**
   * @test {ValidationService#isString}
   * @test {ValidationService#isUrl}
   * @test {ValidationService#isEmail}
   * @test {ValidationService#isStrongPassword}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate strings', async () => {
    const stringValidator = validation.isString()
    const urlValidator = validation.isUrl()
    const emailValidator = validation.isEmail()
    const strongPasswordValidator = validation.isStrongPassword()

    stringValidator.validate('string')
    urlValidator.validate('http://some.url')
    emailValidator.validate('some@email.address')
    strongPasswordValidator.validate('strONGpa33word!!@$')

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [stringValidator, urlValidator, emailValidator, strongPasswordValidator],
          [2105, 'not url', 'not email', 'not strong password']
        )
      },
      '1: Incorrect type. Expected string | 2: Invalid value. Value must be an url | 3: Invalid value. Wrong email syntax | 4: Invalid value. Strong password must have symbos, such as ! " ? $ % ^ & * ( ) _ - + = { [ } ] : ; @ \' ~ # | < , > . ? /',
      [
        { message: 'Incorrect type. Expected string', value: 2105, path: '' },
        { message: 'Invalid value. Value must be an url', value: 'not url', path: '' },
        { message: 'Invalid value. Wrong email syntax', value: 'not email', path: '' },
        { message: 'Invalid value. Strong password must have symbos, such as ! " ? $ % ^ & * ( ) _ - + = { [ } ] : ; @ \' ~ # | < , > . ? /', value: 'not strong password', path: '' }
      ]
    )
  })

  /**
   * @test {ValidationService#isObject}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate objects', async () => {
    const validator1 = validation.isObject({
      required: { prop1: validation.isString() },
      rest: { value: validation.isNumber() }
    })
    const validator2 = validation.isObject({
      optional: { prop2: validation.isString() },
      rest: { key: validation.isEmail() }
    })

    validator1.validate({ prop1: 'string', anything: 54 })
    validator1.validate({ prop1: 'string', anythingswdqw: 52584 })
    validator2.validate({ prop2: 'string', 'shoudl@be.email': 'any' })
    validator2.validate({ 'shoudl_some@be.email': 52584 })

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [validator1, validator1, validator1, validator2, validator2],
          [
            { prop1: 'string', anything: 'wrong_value' },
            { prop1: 54, anything: 54 },
            {},
            { prop2: 'string', wrong_value: 'string' },
            { prop2: 41 }
          ]
        )
      },
      '1: Invalid object parameters | 2: Invalid object parameters | 3: Invalid object parameters | 4: Invalid object parameters | 5: Invalid object parameters',
      [
        { message: 'Incorrect type. Expected number', value: 'wrong_value', path: '.anything' },
        { message: 'Incorrect type. Expected string', value: 54, path: '.prop1' },
        { message: 'Value must exist', value: undefined, path: '.prop1' },
        { message: 'Invalid value. Wrong email syntax', value: 'wrong_value', path: '.wrong_value{key}' },
        { message: 'Incorrect type. Expected string', value: 41, path: '.prop2' }
      ]
    )
  })

  /**
   * @test {ValidationService#isArray}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate arrays', async () => {
    const validator = validation.isArray({ length: validation.isInRange(3, 8), element: validation.isUrl() })

    validator.validate(['http://some.url1', 'http://some.url2', 'http://some.url3'])

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [validator, validator],
          [
            ['http://some.url'],
            ['http://some.url1', 'http://some.url2', 'http://some.url3', 'not_an_url']
          ]
        )
      },
      '1: Invalid array elements | 2: Invalid array elements',
      [
        { message: 'Invalid value. Value must be in range: [3,8]', value: 1, path: '.length' },
        { message: 'Invalid value. Value must be an url', value: 'not_an_url', path: '[3]' }
      ]
    )
  })

  /**
   * @test {ValidationService#isNumber}
   * @test {ValidationService#isInRange}
   * @test {ValidationService#isInteger}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate numbers', async () => {
    const numberValidator = validation.isNumber()
    const inRangeValidator = validation.isInRange(10, 100)
    const integerValidator = validation.isInteger()

    numberValidator.validate(21)
    inRangeValidator.validate(45)
    integerValidator.validate(54)

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [numberValidator, inRangeValidator, integerValidator],
          ['string', 9999, 515.5456]
        )
      },
      '1: Incorrect type. Expected number | 2: Invalid value. Value must be in range: [10,100] | 3: Invalid value. Value must be an integer',
      [
        { message: 'Incorrect type. Expected number', value: 'string', path: '' },
        { message: 'Invalid value. Value must be in range: [10,100]', value: 9999, path: '' },
        { message: 'Invalid value. Value must be an integer', value: 515.5456, path: '' }
      ]
    )
  })

  /**
   * @test {ValidationService#isNotType}
   * @test {ValidationService#isType}
   * @test {ValidationService#isValue}
   * @test {ValidationService#isBoolean}
   * @test {ValidationService#isRegexp}
   * @test {ValidationService#exist}
   * @test {ValidationService#isNull}
   * @test {ValidationService#isUndefined}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate different values', async () => {
    const notTypeValidator = validation.isNotType('string')
    const typeValidator = validation.isType('number')
    const valueValidator = validation.isValue(true)
    const booleanValidator = validation.isBoolean()
    const regexpValidator = validation.isRegexp()
    const existValidator = validation.exist()
    const nullValidator = validation.isNull()
    const undefinedValidator = validation.isUndefined()

    notTypeValidator.validate(false)
    typeValidator.validate(21)
    valueValidator.validate(true)
    booleanValidator.validate(true)
    regexpValidator.validate(/regexp/)
    existValidator.validate({})
    nullValidator.validate(null)
    undefinedValidator.validate(undefined)

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [notTypeValidator, typeValidator, valueValidator, booleanValidator, regexpValidator, existValidator, nullValidator, undefinedValidator],
          ['string', 'not_number', 90, {}, [], null, undefined, null]
        )
      },
      '1: Incorrect type. Expected NOT string | 2: Incorrect type. Expected number | 3: Value must strict equal: true | 4: Invalid value. Logic operator AND failed | 5: Invalid value. Logic operator AND failed | 6: Value must exist | 7: Incorrect type. Expected null | 8: Incorrect type. Expected undefined',
      [
        { message: 'Incorrect type. Expected NOT string', value: 'string', path: '' },
        { message: 'Incorrect type. Expected number', value: 'not_number', path: '' },
        { message: 'Value must strict equal: true', value: 90, path: '' },
        { message: 'Incorrect type. Expected boolean', value: {}, path: '' },
        { message: 'Incorrect type. Expected regexp', value: [], path: '' },
        { message: 'Value must exist', value: null, path: '' },
        { message: 'Incorrect type. Expected null', value: undefined, path: '' },
        { message: 'Incorrect type. Expected undefined', value: null, path: '' }
      ]
    )
  })

  /**
   * @test {ValidationService#AND}
   * @test {ValidationService#OR}
   * @test {ValidationService#isOptional}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate with some logic', async () => {
    const andValidator = validation.AND(validation.isInRange(5, 10), validation.isInteger())
    const orValidator = validation.OR(validation.isString(), validation.isNumber())
    const optionalValidator = validation.isOptional(validation.isObject())

    andValidator.validate(7)
    orValidator.validate('string')
    orValidator.validate(42)
    optionalValidator.validate(undefined)
    optionalValidator.validate({})

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [andValidator, andValidator, orValidator, optionalValidator],
          [8.165, 42, true, []]
        )
      },
      '1: Invalid value. Logic operator AND failed | 2: Invalid value. Logic operator AND failed | 3: Invalid value. Logic operator OR failed | 4: Invalid value. Optional value must be valid or undefined',
      [
        { message: 'Invalid value. Value must be an integer', value: 8.165, path: '' },
        { message: 'Invalid value. Value must be in range: [5,10]', value: 42, path: '' },
        { message: 'Incorrect type. Expected string', value: true, path: '' },
        { message: 'Incorrect type. Expected number', value: true, path: '' },
        { message: 'Incorrect type. Expected object', value: [], path: '' }
      ]
    )
  })

  /**
   * @test {ValidationService#customTest}
   * @test {ValidationService#customValidator}
   * @test {ValidationService#executeValidatorsWithValues}
   * @test {ValidationService#combinedValidationErrors}
   */
  it('should validate with custom logic', async () => {
    const customTestValidator = validation.customTest((value, path) => {
      if (value.property === 45) return true
      else return false
    })
    const customValidator = validation.customValidator((value, path = '') => {
      if (value !== 'something') {
        const message = 'Value must equal - somthing'
        const details = [{ message, value, path }]
        throw new validation.ValidationError(message, details)
      }
    })

    customTestValidator.validate({ property: 45 })
    customValidator.validate('something')

    validatorThrow(
      () => {
        validation.executeValidatorsWithValues(
          [customTestValidator, customValidator],
          [44, 'not_somthing']
        )
      },
      '1: Invalid value. Custom test failed | 2: Value must equal - somthing',
      [
        { message: 'Invalid value. Custom test failed', value: 44, path: '' },
        { message: 'Value must equal - somthing', value: 'not_somthing', path: '' }
      ]
    )
  })

  /**
   * @test {ValidationService#handleValidationError}
   */
  it('should handle ValidationError', async () => {
    try {
      throw new ValidationError('error')
    } catch (e) {
      validation.handleValidationError(e)
    }

    try {
      throw new Error('error')
    } catch (e) {
      ;(() => {
        validation.handleValidationError(e)
      }).should.throw(e)
    }
  })
})
