/* global describe, it, beforeEach, afterEach */
import should from 'should'
import sinon from 'sinon'
import request from 'supertest-promised'
import fse from 'fs-extra'
import path from 'path'

import ExpressService from './express.service'

const expressService = new ExpressService()
const logger = { createLogger () { return { info () {}, warn () {} } } }
expressService._depencencies = expressService._d = { logger }
expressService.init()

const sandbox = sinon.createSandbox()

const testFileName = 'file.temp'
const testFilePath = path.resolve(__dirname, testFileName)

const server = { close () {} }

/**
 * @test {ExpressService}
 */
describe('ExpressService', () => {
  beforeEach(() => {
    sandbox.stub(server, 'close').callsFake((callback) => callback())
    sandbox.stub(expressService.app, 'listen').callsFake((port, callback) => {
      callback()
      return server
    })
  })
  afterEach(async () => {
    sandbox.restore()
    try {
      await fse.remove(testFilePath)
    } catch (e) {
      // ignore
    }
  })

  /**
   * @test {ExpressService#registerRoute}
   */
  it('should register express route', async () => {
    sandbox.stub(expressService.app, 'use')

    const route = '/path/to/smth'
    const router1 = () => {}
    const router2 = () => {}

    expressService.registerRoute(route, router1, router2)

    sinon.assert.calledOnce(expressService.app.use)
    sinon.assert.calledWithExactly(expressService.app.use, route, router1, router2)
  })

  /**
   * @test {ExpressService#registerStatic}
   */
  it('should register express static', async () => {
    const route = '/path/to/smth1'
    const file = 'content'
    await fse.writeFile(testFilePath, file, 'utf8')
    const options = { setHeaders (res) { res.header('content-type', 'text/plain') } }

    expressService.registerStatic(route, __dirname, options)

    const responce = await request(expressService.app)
      .get(`${route}/${testFileName}`)
      .expect(200)
      .end()
      .get('text')

    responce.should.eql(file)
  })

  /**
   * @test {ExpressService#registerStaticFile}
   */
  it('should register express route that returns file', async () => {
    const route = '/path/to/smth2'
    const file = 'content'
    await fse.writeFile(testFilePath, file, 'utf8')
    const options = { headers: { 'content-type': 'text/plain' } }

    expressService.registerStaticFile(route, testFilePath, options)

    const responce = await request(expressService.app)
      .get(route)
      .expect(200)
      .end()
      .get('text')

    responce.should.eql(file)
  })

  /**
   * @test {ExpressService#registerStaticFile}
   */
  it('should register express route that thows an error if file was found or smth else', async () => {
    const route = '/path/to/smth3'
    const file = 'content'
    await fse.writeFile(testFilePath, file, 'utf8')
    const options = {}

    expressService.registerStaticFile(route, '/not.found', options)

    await request(expressService.app)
      .get(route)
      .expect(404)
      .end()
  })

  /**
   * @test {ExpressService#registerAccessControl}
   */
  it('should register express access control route', async () => {
    const route = '/path/to/smth4'
    const route2 = '/path/to/smth5'
    const options2 = {
      methods: ['GET', 'PUT'],
      origins: ['http://site.that.there.is.no.need.to.put.here.because.of.*', '*'],
      headers: ['Profile-Token', 'Custom-Header']
    }

    expressService.registerAccessControl(route)
    expressService.registerRoute(route, (req, res) => res.sendStatus(200))

    expressService.registerAccessControl(route2, options2)
    expressService.registerRoute(route2, (req, res) => res.sendStatus(200))

    const responce = await request(expressService.app)
      .get(route)
      .expect(200)
      .end()

    responce.headers.should.containDeep({
      'access-control-allow-origin': '',
      'access-control-allow-methods': '',
      'access-control-allow-headers': ''
    })

    const responce2 = await request(expressService.app)
      .get(route2)
      .expect(200)
      .end()

    responce2.headers.should.containDeep({
      'access-control-allow-origin': '*',
      'access-control-allow-methods': 'GET, PUT',
      'access-control-allow-headers': 'Profile-Token, Custom-Header'
    })
  })

  /**
   * @test {ExpressService#start}
   */
  it('should start a server', async () => {
    const port = 10000

    await expressService.start(port)
    await expressService.close()

    sinon.assert.calledOnce(expressService.app.listen)
    sinon.assert.calledWith(expressService.app.listen, port)
  })

  /**
   * @test {ExpressService#close}
   */
  it('should close a server if it exist', async () => {
    await expressService.start(1000)
    await expressService.close()

    sinon.assert.calledOnce(server.close)

    sandbox.reset()

    await expressService.close()

    sinon.assert.notCalled(server.close)
  })

  /**
   * @test {ExpressService#createRouter}
   */
  it('should create a router', async () => {
    const router = expressService.createRouter()
    should.exist(router)
  })

  /**
   * @test {ExpressService#bodyParser}
   */
  it('should retunr bodyParser', async () => {
    should.exist(expressService.bodyParser)
  })

  /**
   * @test {ExpressService#express}
   */
  it('should retunr express instance', async () => {
    should.exist(expressService.express)
  })

  describe('Swagger', () => {
    beforeEach(() => {
      expressService._swaggerSchemas = {}
    })

    /**
     * @test {ExpressService#registerSwaggerRoute}
     */
    it('should register swagger route', async () => {
      const schemas = { Schema1: {} }

      const path = '/path'
      const spec = {
        get: {
          description: 'route',
          responses: {
            '200': {
              description: 'route',
              schema: {
                $ref: '#/definitions/Schema1'
              }
            }
          }
        }
      }

      const info = {
        title: 'REST API',
        license: {
          name: 'Copyrights Harno Soft, LLC, 2019. All rights reserved.'
        },
        version: '0.0.1',
        basePath: '/path'
      }
      const route = '/swagger.json'

      expressService.addSwaggerSchema('Schema1', schemas.Schema1)

      const router = expressService.createRouter()
      router.route(path).spec(spec).get(() => {})

      expressService.registerRoute('/', router)
      expressService.registerSwaggerRoute(route, info)

      const responce = await request(expressService.app)
        .post(route)
        .expect(200)
        .end()
        .get('body')

      responce.info.should.be.eql(info)
      responce.basePath.should.be.eql(info.basePath)
      responce.paths.should.be.eql({ [path]: spec })
      responce.definitions.should.containDeep({
        Schema1: schemas.Schema1,
        Error: {
          type: 'object',
          required: ['id', 'error', 'message']
        }
      })
    })

    /**
     * @test {ExpressService#addSwaggerErrorSchema}
     */
    it('should add Error schema', () => {
      expressService.addSwaggerErrorSchema()

      const docs = expressService.generateSwaggerDocs({})

      docs.definitions.should.containDeep({
        Error: {
          type: 'object',
          required: ['id', 'error', 'message']
        }
      })
    })

    /**
     * @test {ExpressService#generateSwaggerDocs}
     */
    it('should generate swagger docs', () => {
      const path = '/path2'
      const spec = {
        get: {
          summary: 'route',
          description: 'route',
          parameters: [],
          responses: {
            '200': {
              description: 'route',
              schema: {
                $ref: '#/definitions/RouteSchema'
              }
            }
          }
        }
      }

      const router = expressService.createRouter()
      router.route(path).spec(spec).get(() => {})
      expressService.registerRoute('/', router)

      const docs = expressService.generateSwaggerDocs({})

      docs.paths.should.containDeep({ [path]: spec })
    })

    /**
     * @test {ExpressService#addSwaggerSchema}
     */
    it('should register schemas', () => {
      const schemas = { Schema1: {}, Schema2: {}, Schema3: {} }

      expressService.addSwaggerSchema('Schema1', schemas.Schema1)
      expressService.addSwaggerSchema('Schema2', schemas.Schema2)
      expressService.addSwaggerSchema('Schema3', schemas.Schema3)

      const docs = expressService.generateSwaggerDocs({})

      docs.definitions.should.be.eql(schemas)
    })
  })
})
