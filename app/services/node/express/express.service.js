import SwaggerGenerator from 'swagger-2.0-node-express-4.x' // npm module swagger-2.0-node-express-4.x should be required before express
import express from 'express'
import bodyParser from 'body-parser'

/**
 * Class that manages express
 */
export default class ExpressService {
  /**
   * Inits logger service and express app
   */
  init () {
    this._app = express()
    this._logger = this._d.logger.createLogger('ExpressService')
    this._swaggerSchemas = {}
  }

  /**
   * Registers express route
   * @param {string|Object} route express route
   * @param {...Function} routers express router
   */
  registerRoute (route, ...routers) {
    this._app.use(route, ...routers)
  }

  /**
   * Registers express static folder
   * @param {string|Object} route express route
   * @param {string} path path to folder or file
   * @param {Object} options express static options
   */
  registerStatic (route, path, options) {
    this.registerRoute(route, express.static(path, options))
  }

  /**
   * Registers express static file
   * @param {string|Object} route express route
   * @param {string} path path to folder or file
   * @param {Object} options express send file options
   */
  registerStaticFile (route, path, options) {
    this.registerRoute(route, (req, res, next) => {
      res.sendFile(path, options, (err) => {
        if (err) next(err)
      })
    })
  }

  /**
   * Registers access control headers
   * @param {string|Object} route express route
   * @param {Object} [options={}] access control options
   * @param {Array<string>} [options.methods=[]] allow methodssss
   * @param {Array<string>} [options.origins=[]] allow origins
   * @param {Array<string>} [options.headers=[]] allow headers
   */
  registerAccessControl (route, { methods = [], origins = [], headers = [] } = {}) {
    this.registerRoute(route, (req, res, next) => {
      let allowOrigin = ''
      for (const origin of origins) {
        if (origin === req.get('Origin') || origin === '*') {
          allowOrigin = origin
          break
        }
      }

      res.header('Access-Control-Allow-Origin', allowOrigin)
      res.header('Access-Control-Allow-Methods', methods.join(', '))
      res.header('Access-Control-Allow-Headers', headers.join(', '))
      next()
    })
  }

  /**
   * Registers express router for swagger documentation
   * @param {string} route express route path
   * @param {Object} info swagger documentation info
   */
  registerSwaggerRoute (route, info) {
    this.addSwaggerErrorSchema()
    const swaggerDocs = this.generateSwaggerDocs(info)

    this.registerRoute(route, (req, res) => {
      res.json(swaggerDocs)
    })
  }

  /**
   * Adds swagger schema
   * @param {string} name schema name
   * @param {Object} schema schema
   */
  addSwaggerSchema (name, schema) {
    this._swaggerSchemas[name] = schema
  }

  /**
   * Generates swagger docs in json
   * @param {Object} info swagger documentation info
   * @param {Object} schemas object of swagger schemas
   * @return {Object} swagger docs in json
   */
  generateSwaggerDocs (info, schemas) {
    const docs = new SwaggerGenerator()

    docs
      .addInfo(info || {})
      .setBasePath(info.basePath || '/')
      .addDefinitions(this._swaggerSchemas)
      .readResources(this.app._router)

    return docs.generateDoc()
  }

  /**
   * Starts express server
   * @param {number|string} port port to listen to
   * @return {Promise} resolves when server is ready
   */
  start (port) {
    return new Promise((resolve) => {
      this._server = this._app.listen(
        port,
        () => {
          this._logger.info(`Working on ${port} port`)
          resolve()
        }
      )
    })
  }

  /**
   * Closes express server
   * @return {Promise} resolves when server is closed
   */
  async close () {
    if (!this._server) {
      this._logger.warn(`Server wasnt created, nothing to close`)
      return
    }
    await new Promise((resolve) => {
      this._server.close(() => {
        resolve()
      })
    })
    delete this._server
  }

  /**
   * Gets express instance
   * @return {Object} express instance
   */
  get express () {
    return express
  }

  /**
   * Gets express app
   * @return {Object} express app
   */
  get app () {
    return this._app
  }

  /**
   * Gets json body parser express middleware
   * @return {Function} json body parser express middleware
   */
  get bodyParser () {
    return bodyParser
  }

  /**
   * Creates express router
   * @return {Object} express router
   */
  createRouter () {
    return express.Router()
  }

  /**
   * Adds swagger error schema to schemas
   */
  addSwaggerErrorSchema () {
    let baseErrorSchema = {
      type: 'object',
      required: [
        'id', 'error', 'message'
      ],
      properties: {
        id: {
          description: 'Error id',
          type: 'integer'
        },
        error: {
          description: 'Error name',
          type: 'string'
        },
        message: {
          description: 'Error description',
          type: 'string'
        },
        details: {
          description: 'Additional information about error',
          type: 'object'
        },
        code: {
          description: 'Error code, used for i18n',
          type: 'string'
        },
        arguments: {
          description: 'Error message arguments, used for i18n',
          type: 'array',
          items: {
            type: 'object'
          }
        }
      }
    }

    this.addSwaggerSchema('Error', baseErrorSchema)
  }
}
