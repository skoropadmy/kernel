/* global describe, it, afterEach */
import 'should'
import sinon from 'sinon'

import MongoDBService from './mongoDB.service'

const mongoDBService = new MongoDBService()
const logger = { createLogger () { return { info () {}, warn () {}, error () {} } } }
mongoDBService._depencencies = mongoDBService._d = { logger }
mongoDBService.init()

const sandbox = sinon.createSandbox()

const dbUrl = 'mongodb://db.url'

/**
 * @test {MongoDBService}
 */
describe('MongoDBService', () => {
  afterEach(() => {
    sandbox.restore()
  })

  /**
   * @test {MongoDBService#mongoose}
   */
  it('should get mongoose', () => {
    const mongoose = mongoDBService.mongoose
    mongoose.should.be.type('object')
  })

  /**
   * @test {MongoDBService#connect}
   */
  it('should connect to db', async () => {
    sandbox.stub(mongoDBService.mongoose, 'connect').resolves()

    await mongoDBService.connect(dbUrl)

    sinon.assert.calledOnce(mongoDBService.mongoose.connect)
    sinon.assert.calledWithExactly(mongoDBService.mongoose.connect, dbUrl, { useNewUrlParser: true })
  })

  /**
   * @test {MongoDBService#tryToConnect}
   */
  it('should try to connect to db', async () => {
    sandbox.stub(mongoDBService.mongoose, 'connect').resolves()

    await mongoDBService.tryToConnect(dbUrl)

    sinon.assert.calledOnce(mongoDBService.mongoose.connect)
    sinon.assert.calledWithExactly(mongoDBService.mongoose.connect, dbUrl, { useNewUrlParser: true })
  })

  /**
   * @test {MongoDBService#tryToConnect}
   */
  it('should try to connect to db even if connection failed a few times', async () => {
    const error = new Error('Mongoose connection error')

    sandbox.stub(mongoDBService.mongoose, 'connect')
      .onCall(0).rejects(error)
      .onCall(1).rejects(error)
      .onCall(2).rejects(error)
      .onCall(3).rejects(error)
      .onCall(4).rejects(error)
      .onCall(5).rejects(error)
      .onCall(6).rejects(error)
      .onCall(7).resolves()

    await mongoDBService.tryToConnect(dbUrl, { attempts: 10, delay: 30 })

    sandbox.assert.callCount(mongoDBService.mongoose.connect, 8)
    sandbox.assert.calledWithExactly(mongoDBService.mongoose.connect, dbUrl, { useNewUrlParser: true })

    sandbox.resetHistory()

    await mongoDBService.tryToConnect(dbUrl, { attempts: 5, delay: 30 }).should.be.rejectedWith(error)

    sandbox.assert.callCount(mongoDBService.mongoose.connect, 5)
    sandbox.assert.calledWithExactly(mongoDBService.mongoose.connect, dbUrl, { useNewUrlParser: true })
  })

  /**
   * @test {MongoDBService#connect}
   */
  it('should throw an error if mongoose unable to connect to db', async () => {
    const dbError = new Error('error')

    sandbox.stub(mongoDBService.mongoose, 'connect').rejects(dbError)

    await mongoDBService.connect(dbUrl).should.be.rejectedWith(dbError)

    sinon.assert.calledOnce(mongoDBService.mongoose.connect)
    sinon.assert.calledWithExactly(mongoDBService.mongoose.connect, dbUrl, { useNewUrlParser: true })
  })

  /**
   * @test {MongoDBService#disconnect}
   */
  it('should close all db connections', async () => {
    sandbox.stub(mongoDBService.mongoose, 'disconnect').resolves()

    await mongoDBService.disconnect()

    sinon.assert.calledOnce(mongoDBService.mongoose.disconnect)
  })

  /**
   * @test {MongoDBService#disconnect}
   */
  it('should throw an error if mongoose unable to close all db connections', async () => {
    const dbError = new Error('error')

    sandbox.stub(mongoDBService.mongoose, 'disconnect').rejects(dbError)

    await mongoDBService.disconnect(dbUrl).should.be.rejectedWith(dbError)

    sinon.assert.calledOnce(mongoDBService.mongoose.disconnect)
  })

  /**
   * @test {MongoDBService}
   */
  it.skip('should create document in db', async () => {
    const mongoDBUrl = 'mongodb://127.0.0.1:27017/test'
    const Schema = mongoDBService.mongoose.Schema

    const testSchema = new Schema({ testField: { type: String, required: true } })

    testSchema.statics.findByTestField = function (testField) {
      return this.find({ testField })
    }

    delete mongoDBService.mongoose.connection.models.Test45
    const Test45 = mongoDBService.mongoose.model('Test45', testSchema)

    try {
      await mongoDBService.connect(mongoDBUrl)

      const randomString = 'test' + Math.random()
      await Test45({ testField: randomString }).save() // creates document in mongodb
      const testObject = (await Test45.findByTestField(randomString))[0].toObject() // gets document from mongodb and converts it to the object

      testObject.should.containDeep({ testField: randomString })
    } finally {
      await mongoDBService.disconnect()
    }
  })
})
