import mongoose from 'mongoose'

/**
 * Class that manages mongo database with mongoose npm module
 */
export default class MongoDBService {
  /**
   * Inits service
   */
  init () {
    this._logger = this._d.logger.createLogger('MongoDBService')
    this._attempts = 0
  }

  /**
   * Tries to connect to mongoDB
   * @param {string} url mongoDB url
   * @param {Object} [options={}] connection options
   * @param {number} [options.attempts=10] connection attempts
   * @param {number} [options.delay=30000] attempts delay in ms
   * @return {Promise<Object>} resolves with mongoose connection object when db is connected
   */
  async tryToConnect (url, { attempts = 10, delay = 30000 } = {}) {
    while (true) {
      try {
        this._attempts++
        const connection = await this._connect(url)
        this._attempts = 0
        this._logger.info(`Mongoose was successfully connected to database ${url}`)
        return connection
      } catch (e) {
        if (this._attempts >= attempts) {
          this._logger.error(`Mongoose wasn't able to connect to database ${url}`, e)
          this._attempts = 0
          throw e
        } else {
          this._logger.warn(`Mongoose wasn't able to connect to database ${url}. Retrying.`, e)
        }

        await new Promise(resolve => setTimeout(resolve, delay))
      }
    }
  }

  /**
   * Connects to mongoDB
   * @param {string} url mongoDB url
   * @return {Promise<Object>} resolves with mongoose connection object when db is connected
   */
  async connect (url) {
    try {
      const connection = await this._connect(url)
      this._logger.info(`Mongoose was successfully connected to database ${url}`)
      return connection
    } catch (e) {
      this._logger.error(`Mongoose wasn't able to connect to database ${url}`, e)
      throw e
    }
  }

  /**
   * Gets mongoose
   * @return {object} mongoose
   */
  get mongoose () {
    return mongoose
  }

  /**
   * Desconnects from mongoDB
   * @return {Promise} resolves when all connections closed
   */
  async disconnect () {
    try {
      return await mongoose.disconnect()
    } catch (e) {
      this._logger.error(`Mongoose wasn't able to close all database connections`, e)
      throw e
    }
  }

  /**
  * Connects to mongoDB
  * @param {string} url mongoDB url
  * @return {Promise<Object>} resolves with mongoose connection object when db is connected
  */
  async _connect (url) {
    return mongoose.connect(url, { useNewUrlParser: true })
  }
}
