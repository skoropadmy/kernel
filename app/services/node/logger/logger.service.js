import log4js from 'log4js'

/**
 * Class that helps with logging
 */
export default class LoggerService {
  /**
   * Configures log4js npm module
   * @param {Object} config log4js config
   */

  init (config) {
    const newConfig = {
      appenders: {
        out: {
          type: 'stdout'
        },
        app: {
          type: 'file',
          filename: config.filename
        }
      },
      categories: {
        default: {
          appenders: config.appenders || [ 'out', 'app' ],
          level: config.level || 'all'
        }
      }
    }
    log4js.configure(newConfig)
  }
  /**
   * Creates logger with log4js
   * @param {name} name logger name
   * @return {Object} logger
   */
  createLogger (name) {
    return log4js.getLogger(name)
  }
}
