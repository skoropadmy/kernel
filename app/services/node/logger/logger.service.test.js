/* global describe, it, after */
import 'should'
import fs from 'fs-extra'

import LoggerService from './logger.service'

const loggerService = new LoggerService()

const delay = (time) => new Promise(resolve => setTimeout(resolve, time))

const file = 'temp/file.for.logger.service.test.txt'

/**
 * @test {LoggerService}
 */
describe('LoggerService', () => {
  after(async () => {
    await fs.unlink(file)
  })

  /**
   * @test {LoggerService}
   */
  it('should configure and return logger', async () => {
    loggerService.init({
      filename: file,
      level: 'all',
      appenders: [ 'out', 'app' ]
    })

    const logger = loggerService.createLogger('LoggerService')
    logger.trace('Entering testing')
    logger.debug('Debug message')
    logger.info(42)
    logger.warn([ 'bread', 'butter' ])
    logger.error({ 'err': 'yes' })
    logger.fatal(['Fatal error message', { 'situation': 'bad' }, 0])

    await delay(100)

    const content = await fs.readFile(file, 'utf-8')

    let combinedRegExp = (`[^]*\\[TRACE\\] LoggerService \\- Entering testing` +
                          `[^]*\\[DEBUG\\] LoggerService \\- Debug message` +
                          `[^]*\\[INFO\\] LoggerService \\- 42` +
                          `[^]*\\[WARN\\] LoggerService \\- \\[ 'bread'\\, 'butter' \\]` +
                          `[^]*\\[ERROR\\] LoggerService \\- \\{ err: 'yes' \\}` +
                          `[^]*\\[FATAL\\] LoggerService \\- \\[ 'Fatal error message'\\, \\{ situation: 'bad'\\ }\\, 0 \\]`)
    combinedRegExp = new RegExp(combinedRegExp)
    const match = combinedRegExp.test(content)
    match.should.be.exactly(true)
  })
})
