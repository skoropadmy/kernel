/* global describe, it, before, afterEach, beforeEach */
import AuthRequestsService from './auth.requests.service'
import sinon from 'sinon'

const authRequests = new AuthRequestsService()
const requestService = { request () {} }
authRequests._depencencies = authRequests._d = {
  request: requestService
}

const sandbox = sinon.createSandbox()
const config = {
  authUrl: 'http://auth.url',
  secretToken: 'secretToken'
}

const user = {
  username: 'some_username',
  email: 'some_email',
  password: 'some_password'
}
const profileToken = 'some_profile_token'

/**
 * @test {AuthRequestsService}
 */
describe('AuthRequestsService', () => {
  before(() => {
    authRequests.init(config)
  })

  beforeEach(() => {
    sandbox.stub(requestService, 'request').resolves()
  })

  afterEach(() => {
    sandbox.restore()
  })

  /**
   * @test {AuthRequestsService#getProfile}
   */
  it('should send request to get profile', async () => {
    await authRequests.getProfile(user.username)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}`,
        method: 'GET',
        headers: { 'Secret-Token': config.secretToken }
      }
    )
  })

  /**
   * @test {AuthRequestsService#createProfile}
   */
  it('should send request to create profile', async () => {
    await authRequests.createProfile(user.username, user.email, user.password)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}`,
        method: 'POST',
        headers: { 'Secret-Token': config.secretToken },
        json: true,
        body: { email: user.email, password: user.password }
      }
    )
  })

  /**
   * @test {AuthRequestsService#updateProfile}
   */
  it('should send request to update profile', async () => {
    await authRequests.updateProfile(user.username, user.email, user.password)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}`,
        method: 'PUT',
        headers: { 'Secret-Token': config.secretToken },
        json: true,
        body: { email: user.email, password: user.password }
      }
    )
  })

  /**
   * @test {AuthRequestsService#deleteProfile}
   */
  it('should send request to delete profile', async () => {
    await authRequests.deleteProfile(user.username)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}`,
        method: 'DELETE',
        headers: { 'Secret-Token': config.secretToken }
      }
    )
  })

  /**
   * @test {AuthRequestsService#createProfileToken}
   */
  it('should send request to create profile token', async () => {
    await authRequests.createProfileToken(user.username, user.password)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}/token`,
        method: 'POST',
        headers: { 'Secret-Token': config.secretToken },
        json: true,
        body: { password: user.password }
      }
    )
  })

  /**
   * @test {AuthRequestsService#validateProfileToken}
   */
  it('should send request to validate profile token', async () => {
    await authRequests.validateProfileToken(user.username, profileToken)

    sinon.assert.calledOnce(requestService.request)
    sinon.assert.calledWithExactly(
      requestService.request,
      {
        url: `${config.authUrl}/profile/${user.username}/validate-token`,
        method: 'POST',
        headers: {
          'Secret-Token': config.secretToken,
          'Profile-Token': profileToken
        }
      }
    )
  })
})
