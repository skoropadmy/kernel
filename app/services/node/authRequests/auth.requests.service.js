/**
 * Class that helps with authorization routes for (harno-auth mostly at this point)
 */
export default class AuthRequestsService {
  /**
   * Sets up authorization variables
   * @param {Object} parameters
   * @param {string} parameters.authUrl authorization endpoint
   * @param {string} parameters.secretToken authorization secter token
   */
  init ({ authUrl, secretToken }) {
    this._authUrl = authUrl
    this._secretToken = secretToken
  }

  /**
   * Gets user profile
   * @param {string} username profile's username
   * @return {Promise<Object>} resolves with user profile
   */
  getProfile (username) {
    const opts = {
      url: `${this._authUrl}/profile/${username}`,
      method: 'GET',
      headers: { 'Secret-Token': this._secretToken }
    }

    return this._d.request.request(opts)
  }

  /**
   * Creates user profile
   * @param {string} username profile's username
   * @param {string} email profile's email
   * @param {string} password profile's password
   * @return {Promise<Object>} resolves when profile is created
   */
  createProfile (username, email, password) {
    const opts = {
      url: `${this._authUrl}/profile/${username}`,
      method: 'POST',
      headers: { 'Secret-Token': this._secretToken },
      json: true,
      body: { email, password }
    }

    return this._d.request.request(opts)
  }

  /**
   * Updates user profile
   * @param {string} username profile's username
   * @param {string} email profile's email
   * @param {string} password profile's password
   * @return {Promise<Object>} resolves when profile is updated
   */
  updateProfile (username, email, password) {
    const opts = {
      url: `${this._authUrl}/profile/${username}`,
      method: 'PUT',
      headers: { 'Secret-Token': this._secretToken },
      json: true,
      body: { email, password }
    }

    return this._d.request.request(opts)
  }

  /**
   * Deletes user profile
   * @param {string} username profile's username
   * @return {Promise<Object>} resolves when profile is deleted
   */
  deleteProfile (username) {
    const opts = {
      url: `${this._authUrl}/profile/${username}`,
      method: 'DELETE',
      headers: { 'Secret-Token': this._secretToken }
    }

    return this._d.request.request(opts)
  }

  /**
   * Creates profile's token
   * @param {string} username profile's username
   * @param {string} password profile's password
   * @return {Promise<Object>} resolves when profile's token is created
   */
  createProfileToken (username, password) {
    const opts = {
      url: `${this._authUrl}/profile/${username}/token`,
      method: 'POST',
      headers: { 'Secret-Token': this._secretToken },
      json: true,
      body: { password }
    }

    return this._d.request.request(opts)
  }

  /**
   * Validates profile's token
   * @param {string} username profile's username
   * @param {string} profileToken profile's token
   * @return {Promise<Object>} resolves with information about token
   */
  validateProfileToken (username, profileToken) {
    const opts = {
      url: `${this._authUrl}/profile/${username}/validate-token`,
      method: 'POST',
      headers: {
        'Secret-Token': this._secretToken,
        'Profile-Token': profileToken
      }
    }

    return this._d.request.request(opts)
  }
}
