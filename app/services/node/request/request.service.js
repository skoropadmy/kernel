import request from 'request-promise-any'
import http from 'http'

/**
 * Class that does different network requests
 */
export default class RequestService {
  /**
   * Makes http(s) request call
   * @param {options} options request options
   * @return {Promise} resolves with an request's responce
   * @throw {Error} if request failed
   */
  async request (options) {
    try {
      return await request(options)
    } catch (e) {
      const error = e.response.toJSON()
      const contentType = error.headers['content-type'] || error.headers['Content-Type'] || 'json'
      if (contentType.includes('json')) {
        const json = typeof error.body === 'string' ? JSON.parse(error.body) : error.body
        error.message = json.message || http.STATUS_CODES[error.statusCode]
        error.details = json.details
      } else {
        error.message = http.STATUS_CODES[error.statusCode]
      }

      this._d.errorHandler.handleRequestError(error)
    }
  }
}
