/* global describe, it */
import 'should'

import RequestService from './request.service'
import ErrorHandlerService from '../errorHandler/error.handler.service'

const requestService = new RequestService()
const errorHandler = new ErrorHandlerService()
const logger = { createLogger () { return { info () {}, warn () {} } } }
errorHandler._depencencies = errorHandler._d = { logger }
requestService._depencencies = requestService._d = { errorHandler }
errorHandler.init()

/**
 * @test {RequestService}
 */
describe('RequestService', () => {
  /**
   * @test {RequestService#request}
   */
  it('should get http request', async function () {
    this.timeout(10000)
    const responce = await requestService.request('http://example.com')
    responce.should.match(/Example Domain/)
  })

  /**
   * @test {RequestService#request}
   */
  it('should handle http request error', async function () {
    this.timeout(10000)
    try {
      await requestService.request('http://example.com/page/that/is/not/found/here/45345').should.be.rejected()
    } catch (actualError) {
      const expectedError = new errorHandler.errors.NotFoundError('Not Found')

      actualError.should.containDeep(expectedError)
    }
  })

  /**
   * @test {RequestService#request}
   */
  it('should handle a rest api request error', async function () {
    this.timeout(10000)
    try {
      await requestService.request({
        url: 'https://www.youtube.com/service_ajax?name=signalServiceEndpoint',
        method: 'POST'
      }).should.be.rejected()
    } catch (actualError) {
      const expectedError = new errorHandler.errors.ValidationError('Bad Request')
      expectedError.body = '{"errors":["Invalid Request"]}'

      actualError.should.eql(expectedError)
    }
  })
})
