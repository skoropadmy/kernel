/**
 * Class that handles different errors
 */
export default class ErrorHandlerService {
  /**
   * Inits logger service and errors classes
   */
  init () {
    /**
     * Defines different error types
     * @type {Object} errors
     * @property {ApiError} error.ApiError
     * @property {NotFoundError} error.NotFoundError
     * @property {ForbiddenError} error.ForbiddenError
     * @property {UnauthorizedError} error.UnauthorizedError
     * @property {ValidationError} error.ValidationError
     * @property {InternalError} error.InternalError
     */
    this.errors = {
      ApiError,
      NotFoundError,
      ForbiddenError,
      UnauthorizedError,
      ValidationError,
      InternalError
    }
    this._logger = this._d.logger.createLogger('ErrorHandlerService')
  }

  /**
   * Returns middleware that handles express errors
   * @return {Function(err, req, res, next)} handles express errors
   */
  get expressErrorHandler () {
    return this._expressErrorHandler.bind(this)
  }

  /**
   * Handles express error
   * @param {Error} err exress error
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  _expressErrorHandler (err, req, res, next) {
    this._logger.error(err)
    res.status(err.status || 500)
    res.json(this._composeErrorResponce(err))
  }

  /**
   * Handles request error
   * @param {Error} e error object
   * @return {Error} new error object
   */
  handleRequestError (e) {
    let error
    let status = e.statusCode || e.status
    if (status === 400) {
      error = new ValidationError(e.message, e.details)
    } else if (status === 401) {
      error = new UnauthorizedError(e.message)
    } else if (status === 403) {
      error = new ForbiddenError(e.message)
    } else if (status === 404) {
      error = new NotFoundError(e.message)
    } else if (status === 500) {
      error = new InternalError(e.message)
    } else {
      error = new ApiError(ApiError, e.message, status)
    }
    error.body = e.body

    throw error
  }

  /**
   * Composes express error responce
   * @param {Error} err error object
   * @return {Error} new error responce
   * @todo this
   */
  _composeErrorResponce (err) {
    const response = {
      error: err.name,
      message: err.message
    }
    if (err.code) {
      response.code = err.code
    }
    if (err.arguments) {
      response.arguments = err.arguments
    }
    if (err.details) {
      response.details = err.details
    }

    return response
  }
}

/**
 * Class for any api error
 */
class ApiError extends Error {
  /**
   * Composes error
   * @param {Class} _class error Class to capture stack trace
   * @param {string} message error message
   * @param {string|number} status error status
   */
  constructor (_class, message, status) {
    super(message)

    this.name = _class.name

    this.status = status
    Error.captureStackTrace(this, _class)
  }
}

/**
 * Class for not found api error
 * @extends Error
 */
class NotFoundError extends ApiError {
  /**
   * Composes error
   * @param {string} message error message
   */
  constructor (message) {
    super(NotFoundError, message, 404)
  }
}

/**
 * Class for forbidden api error
 * @extends Error
 */
class ForbiddenError extends ApiError {
  /**
   * Composes error
   * @param {string} message error message
   */
  constructor (message) {
    super(ForbiddenError, message, 403)
  }
}

/**
 * Class for unauthorizad api error
 * @extends Error
 */
class UnauthorizedError extends ApiError {
  /**
   * Composes error
   * @param {string} message error message
   */
  constructor (message) {
    super(UnauthorizedError, message, 401)
  }
}

/**
 * Class for validation api error
 * @extends Error
 */
class ValidationError extends ApiError {
  /**
   * Composes error
   * @param {string} message error message
   * @param {Object} details validation error details
   */
  constructor (message, details) {
    super(ValidationError, message, 400)
    this.details = details || []
  }
}

/**
 * Class for internal api error
 * @extends Error
 */
class InternalError extends ApiError {
  /**
   * Composes error
   * @param {string} message error message
   */
  constructor (message) {
    super(InternalError, message, 500)
  }
}
