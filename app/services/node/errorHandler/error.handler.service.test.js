/* global describe, it, afterEach, beforeEach */
import 'should'
import sinon from 'sinon'
import ErrorHandlerService from './error.handler.service'

const errorHandler = new ErrorHandlerService()
const logger = { createLogger () { return { error () {} } } }
errorHandler._depencencies = errorHandler._d = { logger }
errorHandler.init()
const errors = errorHandler.errors

const res = {
  status: function () {},
  json: function () {}
}

const sandbox = sinon.createSandbox()
let sendSpy
let statusSpy

const objectValidator = (object, instance, message, status) => {
  if (instance) {
    object.should.be.an.instanceof(instance)
  }
  if (message) {
    object.message.should.be.exactly(message)
  }
  if (status) {
    object.status.should.be.exactly(status)
  }
}

const shouldHandleRequestError = (err, errorClass) => {
  try {
    errorHandler.handleRequestError(err)
    throw new Error('should throw request error')
  } catch (requestError) {
    objectValidator(requestError, errorClass, err.message)
  }
}

/**
 * @test {ErrorHandlerService}
 */
describe('ErrorHandlerService', () => {
  afterEach(() => {
    sandbox.restore()
  })

  beforeEach(() => {
    sendSpy = sandbox.spy(res, 'json')
    statusSpy = sandbox.spy(res, 'status')
  })

  /**
   * @test {ErrorHandlerService#expressErrorHandler}
   */
  it('should take error and write it to response object', () => {
    const err1 = {
      name: 'name1',
      message: 'message1',
      status: 1000
    }

    errorHandler.expressErrorHandler(err1, {}, res)

    sinon.assert.calledOnce(sendSpy)
    sinon.assert.calledWithExactly(sendSpy, { error: 'name1', message: 'message1' })

    sinon.assert.calledOnce(statusSpy)
    sinon.assert.calledWithExactly(statusSpy, 1000)
  })

  /**
   * @test {ErrorHandlerService#expressErrorHandler}
   */
  it('if error object don\'t have status property, status should be equal to 500', () => {
    const err = {
      name: 'name2',
      message: 'message2'
    }

    errorHandler.expressErrorHandler(err, {}, res)

    sinon.assert.calledOnce(sendSpy)
    sinon.assert.calledWithExactly(sendSpy, { error: 'name2', message: 'message2' })

    sinon.assert.calledOnce(statusSpy)
    sinon.assert.calledWithExactly(statusSpy, 500)
  })

  /**
   * @test {ErrorHandlerService#expressErrorHandler}
   */
  it('if error object has code or/and details or/and arguments properties, they will be written to response object', () => {
    const err = {
      name: 'name3',
      message: 'message3',
      status: 404,
      code: 42,
      arguments: ['arg1', { oil: false }],
      details: 'this is error with a lot of properties'
    }

    errorHandler.expressErrorHandler(err, {}, res)

    sinon.assert.calledOnce(sendSpy)
    sinon.assert.calledWithExactly(sendSpy, {
      error: 'name3',
      message: 'message3',
      code: 42,
      arguments: ['arg1', { oil: false }],
      details: 'this is error with a lot of properties'
    })

    sinon.assert.calledOnce(statusSpy)
    sinon.assert.calledWithExactly(statusSpy, 404)
  })

  /**
   * @test {ErrorHandlerService#handleRequestError}
   */
  it('should handle request errors', () => {
    const errors = errorHandler.errors

    shouldHandleRequestError({ status: 404, message: 'someMessage1' }, errors.NotFoundError)
    shouldHandleRequestError({ status: 400, message: 'someMessage2' }, errors.ValidationError)
    shouldHandleRequestError({ status: 401, message: 'someMessage3' }, errors.UnauthorizedError)
    shouldHandleRequestError({ status: 3742, message: 'someMessage5' }, errors.ApiError)
    shouldHandleRequestError({ status: 403, message: 'someMessage6' }, errors.ForbiddenError)
  })

  /**
   * @test {ErrorHandlerService#errors}
   */
  it('should create new errors', () => {
    const notFoundError = new errors.NotFoundError('message1')
    const forbiddenError = new errors.ForbiddenError('message2')
    const unauthorizedError = new errors.UnauthorizedError('message3')
    const validationError = new errors.ValidationError('message4')
    const internalError = new errors.InternalError('message5')
    const apiError = new errors.ApiError(errors.ApiError, 'message6', 42)

    objectValidator(notFoundError, errors.NotFoundError, 'message1', 404)
    objectValidator(forbiddenError, errors.ForbiddenError, 'message2', 403)
    objectValidator(unauthorizedError, errors.UnauthorizedError, 'message3', 401)
    objectValidator(validationError, errors.ValidationError, 'message4', 400)
    objectValidator(internalError, errors.InternalError, 'message5', 500)
    objectValidator(apiError, errors.ApiError, 'message6', 42)
  })
})
