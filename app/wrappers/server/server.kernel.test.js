/* global describe, it, afterEach */
import ServerKernel from './server.kernel'
import 'should'
import sinon from 'sinon'

const serverKernel = new ServerKernel()
const sandbox = sinon.createSandbox()

const Service = class {}

/**
 * @test {ServerKernel}
 */
describe('ServerKernel', () => {
  afterEach(() => {
    serverKernel.serviceManagement.removeAll()
    sandbox.restore()
  })

  /**
   * @test {ServerKernel#registerDefaultServices}
   */
  it('should register default services', async () => {
    sandbox.spy(serverKernel.kernel, 'registerService')

    serverKernel.registerDefaultServices()

    sinon.assert.callCount(serverKernel.kernel.registerService, 10)
    serverKernel.s.should.have.only.keys(
      'jobsScheduler', 'express', 'config', 'mongoDB', 'logger', 'errorHandler',
      'request', 'tick', 'authRequests', 'validation'
    )
  })

  /**
   * @test {ServerKernel#initDefaultServicesDependencies}
   */
  it('should init default services dependencies', async () => {
    serverKernel.registerDefaultServices()

    serverKernel.initDefaultServicesDependencies()

    serverKernel.s.errorHandler._d.should.have.only.keys('logger')
    serverKernel.s.jobsScheduler._d.should.have.only.keys('logger')
    serverKernel.s.mongoDB._d.should.have.only.keys('logger')
    serverKernel.s.tick._d.should.have.only.keys('logger')
    serverKernel.s.express._d.should.have.only.keys('logger')
    serverKernel.s.authRequests._d.should.have.only.keys('request')
  })

  /**
   * @test {ServerKernel#initDefaultServices}
   */
  it('should init default services', async () => {
    serverKernel.registerDefaultServices()
    serverKernel.initDefaultServicesDependencies()

    serverKernel.initDefaultServices()
  })

  /**
   * @test {ServerKernel#registerService}
   */
  it('should register service', async () => {
    sandbox.stub(serverKernel.kernel, 'registerService')

    const serviceName = 'service'
    serverKernel.registerService(serviceName, Service)

    sinon.assert.calledOnce(serverKernel.kernel.registerService)
    sinon.assert.calledWithExactly(serverKernel.kernel.registerService, serviceName, Service)
  })

  /**
   * @test {ServerKernel#getService}
   */
  it('should get service', async () => {
    sandbox.stub(serverKernel.kernel, 'getService')

    const serviceName = 'service'
    serverKernel.getService(serviceName)

    sinon.assert.calledOnce(serverKernel.kernel.getService)
    sinon.assert.calledWithExactly(serverKernel.kernel.getService, serviceName)
  })

  /**
   * @test {ServerKernel#initDependencies}
   */
  it('should init services dependencies', async () => {
    sandbox.stub(serverKernel.kernel, 'initDependencies')

    const dependenciesConfig = { service: [ 'd1', 'd2' ] }
    serverKernel.initDependencies(dependenciesConfig)

    sinon.assert.calledOnce(serverKernel.kernel.initDependencies)
    sinon.assert.calledWithExactly(serverKernel.kernel.initDependencies, dependenciesConfig)
  })
})
