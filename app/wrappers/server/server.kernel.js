import Kernel from '../../kernel'

import ConfigService from '../../services/shared/config/config.service'
import JobsSchedulerService from '../../services/shared/jobsScheduler/jobs.scheduler.service'
import TickService from '../../services/shared/tick/tick.service'
import ValidationService from '../../services/shared/validation/validation.service'
import LoggerService from '../../services/node/logger/logger.service'
import ExpressService from '../../services/node/express/express.service'
import MongoDBService from '../../services/node/mongoDB/mongoDB.service'
import RequestService from '../../services/node/request/request.service'
import ErrorHandlerService from '../../services/node/errorHandler/error.handler.service'
import AuthRequestsService from '../../services/node/authRequests/auth.requests.service'

/**
 * Class that manages a lot of usefull services for node.js
 */
export default class ServerKernel {
  /**
   * Inits kernel
   */
  constructor () {
    this.kernel = new Kernel()
  }

  /**
   * Registers default services
   */
  registerDefaultServices () {
    this.registerService('jobsScheduler', JobsSchedulerService)
    this.registerService('express', ExpressService)
    this.registerService('config', ConfigService)
    this.registerService('mongoDB', MongoDBService)
    this.registerService('logger', LoggerService)
    this.registerService('errorHandler', ErrorHandlerService)
    this.registerService('request', RequestService)
    this.registerService('tick', TickService)
    this.registerService('authRequests', AuthRequestsService)
    this.registerService('validation', ValidationService)
  }

  /**
   * Inits default services dependencies
   */
  initDefaultServicesDependencies () {
    this.initDependencies({
      request: ['errorHandler'],
      errorHandler: ['logger'],
      jobsScheduler: ['logger'],
      mongoDB: ['logger'],
      tick: ['logger'],
      express: ['logger'],
      authRequests: ['request']
    })
  }

  /**
   * Inits default services
   */
  initDefaultServices () {
    this.s.errorHandler.init()
    this.s.jobsScheduler.init()
    this.s.mongoDB.init()
    this.s.tick.init()
    this.s.express.init()

    this.s.validation.ValidationError = this.s.errorHandler.errors.ValidationError
  }

  /**
   * Inits services dependencies
   * @param {Object<string, Array<string>>} dependenciesConfig config that contains all services dependencies
   * @throws {Error} if dependenciesConfig is not an object
   */
  initDependencies (dependenciesConfig) {
    this.kernel.initDependencies(dependenciesConfig)
  }

  /**
   * Registers service
   * @param {string} name service name
   * @param {class} Service service class
   * @throws {Error} if service is alredy defined
   */
  registerService (name, Service) {
    this.kernel.registerService(name, Service)
  }

  /**
   * Gets service
   * @param {string} name service name
   * @return {Object} service
   * @throws {Error} if service is not defined
   */
  getService (name) {
    return this.kernel.getService(name)
  }

  /**
   * Returns services management service
   * @return {Object} services management service
   */
  get serviceManagement () {
    return this.kernel.serviceManagement
  }

  /**
   * Returns all registered services
   * @return {Object} all registered services
   */
  get s () {
    return this.kernel.s
  }
}
