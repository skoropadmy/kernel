/* global describe, it, beforeEach, afterEach */
import 'should'
import Kernel from './kernel'
import sinon from 'sinon'

const kernel = new Kernel()
const serviceManagement = kernel.serviceManagement
const sandbox = sinon.createSandbox()

const Service = class {
  initDependencies () {}
}

/**
 * @test {ServiceManagement}
 */
describe('ServiceManagement', () => {
  beforeEach(() => {
    serviceManagement.removeAll()
  })

  afterEach(() => {
    sandbox.restore()
  })

  /**
   * @test {ServiceManagement#getAll}
   */
  it('should get all services', async () => {
    serviceManagement.register('service', new Service())
    serviceManagement.register('service1', new Service())
    serviceManagement.register('service2', new Service())
    serviceManagement.register('service3', new Service())

    serviceManagement.getAll().should.have.only.keys('service', 'service1', 'service2', 'service3')
  })

  /**
   * @test {ServiceManagement#removeAll}
   */
  it('should remove all services', async () => {
    serviceManagement.register('service', new Service())
    serviceManagement.register('service1', new Service())
    serviceManagement.register('service2', new Service())
    serviceManagement.register('service3', new Service())

    serviceManagement.removeAll()

    serviceManagement.getAll().should.have.only.keys()
  })

  /**
   * @test {ServiceManagement#get}
   */
  it('should get service', async () => {
    serviceManagement.register('service', new Service())

    serviceManagement.get('service').should.be.instanceof(Service)
  })

  /**
   * @test {ServiceManagement#get}
   */
  it('should throw an error if service name is not defined', async () => {
    const error = new Error(`Service "not_defined" is not defined`);
    (() => {
      serviceManagement.get('not_defined')
    }).should.throw(error)
  })

  /**
   * @test {ServiceManagement#register}
   */
  it('should register service', async () => {
    serviceManagement.register('service', new Service())

    serviceManagement.get('service')
  })

  /**
   * @test {ServiceManagement#register}
   */
  it('should throw an error if service is already defined', async () => {
    serviceManagement.register('service2', new Service())

    const error = new Error(`Service "service2" is already defined`);
    (() => {
      serviceManagement.register('service2', new Service())
    }).should.throw(error)
  })

  /**
   * @test {ServiceManagement#initDependencies}
   */
  it('should init service dependencies', async () => {
    const dependency1 = new Service()
    const dependency2 = new Service()
    const service = new Service()

    serviceManagement.register('dependency1', dependency1)
    serviceManagement.register('dependency2', dependency2)
    serviceManagement.register('service', service)

    serviceManagement.initDependencies({
      service: ['dependency1', 'dependency2'],
      dependency2: ['dependency1']
    })

    kernel.s.service._d.should.have.only.keys('dependency1', 'dependency2')
    kernel.s.dependency2._d.should.have.only.keys('dependency1')
  })

  /**
   * @test {ServiceManagement#initDependencies}
   */
  it('should throw an error if dependencies config is not an object, or some of the services is not registered', async () => {
    let error = new Error('Dependencies config must be an object')
    ;(() => {
      serviceManagement.initDependencies(546)
    }).should.throw(error)

    error = new Error('Service "notRegistered" is not defined')
    ;(() => {
      serviceManagement.initDependencies({ notRegistered2: ['notRegistered'] })
    }).should.throw(error)
  })
})
