# Wrappers
We have a bunch of available wrappers, that combines some services together to create full featured environments, so you wouldn't need to create a lot of logic manually each time

---

# Available wrappers

## ServerKernel
Created for web-servers, rest-apis, and anything that uses servers, request and etc


#### Default registered services:
- `jobsScheduler` - JobsSchedulerService
- `express` - ExpressService
- `config` - ConfigService
- `mongoDB` - MongoDBService
- `logger` - LoggerService
- `errorHandler` - ErrorHandlerService
- `request` - RequestService
- `tick` - TickService
- `swagger` - SwaggerService
- `authRequests` - AuthRequestsService
- `validation` - ValidationService

#### Other default things:
- Replaces validation service default error
```js
this.s.validation.ValidationError = this.s.errorHandler.errors.ValidationError
```

#### Basic usage:
###### main.js
```js
// you can export ServerKernel instance
import YourService from './your.file.js'
import ServerKernel from 'harno-kernel/dist/wrappers/server/server.kernel'

export default class App {
  constructor () {
    this.serverKernel = new ServerKernel()
    // you can init all default services and it's dependencies
    this.serverKernel.registerDefaultServices()
    this.serverKernel.initDefaultServicesDependencies()

    // some services have logger service as their dependencies, so before initing them, it would be great to configure logger
    this.serverKernel.getService('logger').config({ ... })

    // call init methods of some services
    this.serverKernel.initDefaultServices()

    // you can register your services like this
    this.serverKernel.registerService('yourAnyName', YourService)

    // you can init dependencies for your service like this
    this.serverKernel.initDependencies({
      yourAnyName: ['express', 'config']
    })

    // you can get any service by its name like this
    const yourAnyName = this.serverKernel.getService('yourAnyName')
    // or
    const yourAnyName = this.serverKernel.s.yourAnyName
  }
}
```

###### your.file.js
```js
export default class YourService {
  someMethod () {
    // after init your service will have property _dependencies or _d, that will contain all inited dependencies
    const express = this._d.express
    const config = this._d.config

    if (express && config) {
      console.log('My dependencies is: express and config')
    }
  }
}
```

---

## Wrappers API
### `new Wrapper()`
Creates new server kernel instance

### `wrapper.initDependencies(dependenciesConfig)`
- dependenciesConfig example:
```js
  const dependenciesConfig = {
    serviceName1: ['serviceName2', 'serviceName3'],
    serviceName2: ['serviceName4'],
    ...
  }
```

Inits/sets services dependencies, so that in our example service `serviceName1` will have property `_dependencies` or `_d` that would contain `serviceName2`, `serviceName3` services

### `wrapper.registerService(name, Service)`
- name - service name (can be any string)
- Service - any js class

Register/saves `Service` class instance under the `name` in the server kernel

### `wrapper.getService(name)`
- name - service name

Gets service that was created under that `name`

### `wrapper.s`

Kernel property that contains all registered services

### `wrapper.registerDefaultServices()`

Registers all default services under default names (look at `Defaut registered services` for more info)

### `wrapper.initDefaultServicesDependencies()`

Inits all default services dependencies

### `wrapper.initDefaultServices()`

Inits all default services. Calls `init` method of default services
