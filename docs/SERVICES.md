# Services
We have a bunch of available services, so you wouldn't need to create them manually each time

---

# Available services

## Node specific services. Folder: `dist/services/node/`

#### AuthRequestsService
- Default name: `authRequests`
- Dependencies: `['request']`
- Description: Class that helps with authorization routes for (harno-auth mostly at this point)
- API: Work in progress, so more reliable api would be announced later

#### ExpressService
- Default name: `express`
- Dependencies: `['logger']`
- Description: Class that manages express
- API: Work in progress, so more reliable api would be announced later

#### MongoDBService
- Default name: `mongoDB`
- Dependencies: `['logger']`
- Description: Class that manages mongo database with mongoose npm module
- API: Work in progress, so more reliable api would be announced later

#### LoggerService
- Default name: `logger`
- Dependencies: `none`
- Description: Class that helps with logging
- API: Work in progress, so more reliable api would be announced later

#### ErrorHandlerService
- Default name: `errorHandler`
- Dependencies: `['logger']`
- Description: Class that handles different errors
- API: Work in progress, so more reliable api would be announced later

#### RequestService
- Default name: `request`
- Dependencies: `['errorHandler']`
- Description: Class that does different network requests
- API: Work in progress, so more reliable api would be announced later

## Shared service. Can be run on anything that supports js. Folder: `dist/services/shared/`

#### JobsSchedulerService
- Default name: `jobsScheduler`
- Dependencies: `['logger']`
- Description: Class that manages jobs (jobs are functions that behave like cron jobs)
- API: Work in progress, so more reliable api would be announced later

#### ConfigService
- Default name: `config`
- Dependencies: `none`
- Description: Class that manages configuration
- API: Work in progress, so more reliable api would be announced later

#### TickService
- Default name: `tick`
- Dependencies: `['logger']`
- Description: Class that manages tick functions (functions that is executed per interval)
- API: Work in progress, so more reliable api would be announced later

#### ValidationService
- Default name: `validation`
- Dependencies: `['errorHandler']`
- Description: Class that validates different values
- API: Work in progress, so more reliable api would be announced later
