# Kernel
Mainly used to combine and manage a lot of different services

## Usage
###### main.js
```js
// you can export Kernel class, or any other available service
import YourService from './your.file.js'
import YourService2 from './your.file2.js'
import Kernel from 'harno-kernel' // or 'harno-kernel/dist/kernel'

export default class App {
  constructor () {
    this.kernel = new Kernel()

    // you can register services
    this.kernel.registerService('yourAnyName', YourService)
    this.kernel.registerService('yourAnyName2', YourService2)

    // you can init dependencies for any service
    this.kernel.initDependencies({
      yourAnyName: ['yourAnyName2']
    })

    // in this example YourService would have property _dependencies or _d with link to yourAnyName2 services
    const yourAnyName = this.kernel.getService('yourAnyName')

    // look at the your.file.js bellow
    console.log(yourAnyName.afterInitDependencies())
    // output: My dependencies is: yourAnyName2
  }

  async start () {
    // you can get services like this
    const yourAnyName2 = this.kernel.getService('yourAnyName2')

    // or like this
    const yourAnyName2 = this.kernel.s.yourAnyName2
  }
}
```

###### your.file.js
```js
export default class YourService {
  afterInitDependencies () {
    // after init your service will have property _dependencies or _d, that will contain all inited dependencies
    const yourAnyName2 = this._d.yourAnyName2

    if (yourAnyName2) {
      console.log('My dependencies is: yourAnyName2')
    }
  }
}
```

###### your.file2.js
```js
export default class YourService2 { }
```

## API
### `new Kernel()`
Creates new kernel instance

### `kernel.initDependencies(dependenciesConfig)`
- dependenciesConfig example:
```js
  const dependenciesConfig = {
    serviceName1: ['serviceName2', 'serviceName3'],
    serviceName2: ['serviceName4'],
    ...
  }
```

Inits/sets services dependencies, so that in our example service `serviceName1` will have property `_dependencies` or `_d` that would contain `serviceName2`, `serviceName3` services

### `kernel.registerService(name, Service)`
- name - service name (can be any string)
- Service - any js class

Register/saves `Service` class instance under the `name` in the kernel

### `kernel.getService(name)`
- name - service name

Gets service that was created under that `name`

### `kernel.s`

Kernel property that contains all registered services
