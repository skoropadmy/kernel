# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Changed
- Simplify `loggerService` init
- Rename and refactor main file

## [0.0.8] - 2019-01-10
### Added
- Move all available services back to the kernel
- Add `server` services wrappers, that contains all services connected together with kernel for server like applications
- Add new methods to `expressService`: `registerStatic`, `registerStaticFile`, `registerAccessControl`,
  `registerSwaggerRoute`, `addSwaggerSchema`, `generateSwaggerDocs`
- Add new method to `mongoDBService`: `tryToConnect`
### Changed
- Change `configService` `setYML` and `setJSON` methods to `setConfigYML` and `setConfigJSON`
- Change kernel and wrappers default exports. Now it is only the main class, not a instance
- Remove all read methods from `configService` to make it web compilable
- Merge `swaggerService` into `expressService`
- Remove `errorHandler` from `validationService` dependencies
- Add setter to `validationService` that let's you change default ValidationError
- Change `swaggerService` method name `init` to `info`
- Improve error handling in `requestService#request`
- Change `errorHandler#handleRequestError`. Now it throws an error
- Change `loggerService` method `config` to `init`
- Change `authRequests` method `configure` to `init`
- Change abbreviations in names to uppercase

## [0.0.7] - 2018-12-27
### Changed
- Remove all default services from this repository to `harno-kernel-server`. Kernel should contain only logic.

## [0.0.6] - 2018-12-21
### Added
- Add validation service, that validates different configurations
- Add all available services to main file export
- Add to service instances property `_dependencies` that equals property `_d`
### Changed
- Move register-external-services-logic to method `registerDefaultExternalServices`, so now services aren't registered by default
- Improve swagger service with new methods
- Improve config service with new methods
### Fixed
- Fix javascript documentation

## [0.0.5] - 2018-12-13
### Added
- `s` property to the kernel, that contains all registered services
- `getService` method to the kernel, that gets service by name
### Changed
- Improve config servcie with new functions (readJson, readYml, setDefaults, ...)
- Change dependencies applying logic (now all dependencies are in `_d` property)
- Change `services` property to `serviceManagement` in kernel
- Change `initDependencies` method, now it takes one object argument that contains all dependencies configuration
- Change `authorization` service name to `authRequests`
- Change project name to `harno-kernel`

## [0.0.4] - 2018-12-07
### Added
- Add services: authorization, config, errorHandler, express, httpClient, jobsScheduler, logger,
  mongoDB, swagger, tick
